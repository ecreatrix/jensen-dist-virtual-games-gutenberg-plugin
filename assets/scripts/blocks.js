"use strict";
(self["webpackChunkjg_virtual_games"] = self["webpackChunkjg_virtual_games"] || []).push([["/assets/scripts/blocks"],{

/***/ "./virtualgames-gutenberg/src/blocks/activity-categories/edit.jsx":
/*!************************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-categories/edit.jsx ***!
  \************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
/* harmony import */ var _components_category__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/category */ "./virtualgames-gutenberg/src/blocks/components/category.jsx");
// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$element = wp.element,
    Component = _wp$element.Component,
    Fragment = _wp$element.Fragment;
var _wp$blockEditor = wp.blockEditor,
    InnerBlocks = _wp$blockEditor.InnerBlocks,
    InspectorControls = _wp$blockEditor.InspectorControls,
    InspectorAdvancedControls = _wp$blockEditor.InspectorAdvancedControls,
    withColors = _wp$blockEditor.withColors,
    useBlockProps = _wp$blockEditor.useBlockProps;
var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner,
    PanelBody = _wp$components.PanelBody,
    PanelRow = _wp$components.PanelRow;

var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select; // Other Dependencies
// Internal dependencies




function ActivityCategory(_ref) {
  var isRequesting = _ref.isRequesting,
      activityCategories = _ref.activityCategories,
      setAttributes = _ref.setAttributes,
      categories = _ref.attributes.categories;

  var renderCategoriesItem = function renderCategoriesItem(category, index) {
    return wp.element.createElement("div", {
      key: index,
      className: "",
      dangerouslySetInnerHTML: {
        __html: category.jg_sport_tax
      }
    });
  };

  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, (0,_components_category__WEBPACK_IMPORTED_MODULE_0__.multipleCategoriesPanel)(setAttributes, categories, activityCategories)));

  if (isRequesting) {
    return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__["default"],
      label: __('Activity Categories')
    }, wp.element.createElement(Spinner, null)));
  }

  var blockProps = useBlockProps({
    className: 'sport-categories'
  });
  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement("div", blockProps, wp.element.createElement("div", {
    className: "activities-row"
  }, activityCategories && activityCategories.map(function (category, index) {
    return renderCategoriesItem(category, index);
  }))));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select) {
  var _select = select('core'),
      getEntityRecords = _select.getEntityRecords;

  var _select2 = select('core/data'),
      isResolving = _select2.isResolving;

  var query = {
    per_page: -1
  };
  return {
    activityCategories: getEntityRecords('taxonomy', 'sport_tax', query),
    isRequesting: isResolving('core', 'getEntityRecords', ['taxonomy', 'jg_sport_tax', query])
  };
}), withColors('backgroundColor', 'overlayColor', 'textColor'))(ActivityCategory));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-categories/index.jsx":
/*!*************************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-categories/index.jsx ***!
  \*************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/activity-categories/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/activity-categories/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
// Internal dependencies



var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"],
  icon: wp.element.createElement("svg", {
    viewBox: "0 0 18 18",
    version: "1.1",
    xmlns: "http://www.w3.org/2000/svg"
  }, wp.element.createElement("path", {
    d: "M16,0 L2,0 C0.9,0 0,0.9 0,2 L0,16 C0,17.1 0.9,18 2,18 L16,18 C17.1,18 18,17.1 18,16 L18,2 C18,0.9 17.1,0 16,0 Z M16.5,16 C16.5,16.3 16.3,16.5 16,16.5 L2,16.5 C1.7,16.5 1.5,16.3 1.5,16 L1.5,4 L16.5,4 L16.5,16 Z"
  }), wp.element.createElement("path", {
    d: "M5.4670641,6.51737308 C5.50560826,6.51737308 5.53685448,6.48612667 5.53685448,6.4475825 C5.53775201,6.28903484 5.65865421,6.15696808 5.81650636,6.14210654 C5.8548768,6.1386481 5.88322848,6.10481606 5.87992245,6.06643219 C5.87604426,6.02832882 5.84261873,6.00018482 5.80441154,6.00285266 C5.57458335,6.02450586 5.39852687,6.21674089 5.39711026,6.4475825 C5.39711026,6.46612041 5.40448554,6.48389669 5.41760915,6.49698963 C5.43073276,6.51008258 5.44852623,6.5174163 5.4670641,6.51737308 L5.4670641,6.51737308 Z"
  }), wp.element.createElement("path", {
    d: "M5.83023562,7.22475648 C6.26661441,7.22448572 6.62017516,6.87055917 6.61999475,6.43418033 C6.6198142,5.99780149 6.26596081,5.64416753 5.82958194,5.64425771 C5.39320307,5.64434792 5.03949582,5.99812814 5.03949582,6.43450702 C5.03985662,6.87099167 5.39375083,7.22466643 5.83023562,7.22475648 L5.83023562,7.22475648 Z M5.24429057,6.31535668 C5.30683112,6.00712845 5.59758888,5.8001378 5.90930897,5.84192963 C6.22102905,5.88372147 6.44699716,6.15998875 6.42614109,6.47380557 C6.40528503,6.78762238 6.14474466,7.03155663 5.83023562,7.03173425 C5.65065415,7.03243258 5.48033192,6.95211272 5.36663401,6.81310632 C5.2529361,6.67409992 5.20798844,6.49123207 5.24429057,6.31535668 L5.24429057,6.31535668 Z"
  }), wp.element.createElement("path", {
    d: "M6.55494422,7.44507836 C6.35178392,7.45390431 6.16169909,7.67586062 6.24603595,7.88735654 C6.35423557,8.15916313 6.06739218,8.28158234 5.86406843,8.10489988 C5.66074468,7.92821743 4.35728253,6.7190622 4.35728253,6.7190622 C4.35728253,6.7190622 4.26902303,6.63521567 4.13598,6.50740283 C4.02810727,6.40525063 3.75548346,6.14488508 3.63224704,6.0281864 C3.51113539,5.91377593 3.24161701,5.66011158 3.14028202,5.56613155 C3.0891242,5.51971359 2.89511673,5.34237736 2.86635067,5.31933182 C2.62265636,5.11519085 2.35183044,5.4007267 2.59568818,5.67253329 C2.62837689,5.70963497 2.85490962,5.94907974 2.96458023,6.06479775 C3.06101191,6.16678652 3.32023334,6.43826622 3.44232566,6.56575217 C3.55166938,6.68016264 3.81514035,6.95360366 3.93445413,7.0776573 C4.7634397,7.93769715 5.08068359,8.24611509 5.38583266,8.52445942 C6.03372281,9.11547122 6.64434783,8.84219364 6.86565037,8.38978195 C7.05769652,7.99735404 6.87937963,7.43151254 6.55494422,7.44507836 Z"
  }), wp.element.createElement("path", {
    d: "M10.2674181,9.18182982 C10.1571551,9.18194715 10.0513647,9.1382429 9.97333466,9.0603375 C9.89530461,8.98243209 9.85143129,8.87671171 9.85137257,8.76644867 L9.85137257,5.69484527 C9.84789189,5.54411718 9.92631408,5.40332493 10.0562992,5.32693816 C10.1862843,5.25055139 10.3474442,5.25055139 10.4774293,5.32693816 C10.6074144,5.40332493 10.6858366,5.54411718 10.6823559,5.69484527 L10.6823559,8.76644867 C10.6822341,8.99563378 10.4966029,9.18146311 10.2674181,9.18182982 Z M12.5713976,6.86256409 L10.8733204,6.86256409 L10.8733204,7.59828677 L12.5716191,7.59828677 L12.5713976,6.86256409 Z M13.5929023,8.76644867 L13.5929023,5.69484527 C13.5929023,5.46531437 13.4068308,5.27924282 13.1772999,5.27924282 C12.947769,5.27924282 12.7616974,5.46531437 12.7616974,5.69484527 L12.7616974,8.76644867 C12.7616974,8.99597956 12.947769,9.18205112 13.1772999,9.18205112 C13.4068308,9.18205112 13.5929023,8.99597956 13.5929023,8.76644867 Z M9.31702657,5.90464032 C9.14031557,5.90476261 8.99712784,6.04804951 8.99712788,6.22476055 L8.99712788,6.86256409 L8.88635963,6.86256409 C8.68313425,6.86256409 8.51838753,7.02731082 8.51838753,7.2305362 C8.51838753,7.43376158 8.68313425,7.59850831 8.88635963,7.59850831 L8.99712788,7.59850831 L8.99712788,8.23609032 C8.99406667,8.35242618 9.05438176,8.46125929 9.15465232,8.5203289 C9.25492288,8.57939852 9.37935179,8.57939852 9.47962235,8.5203289 C9.57989291,8.46125929 9.640208,8.35242618 9.63714679,8.23609032 L9.63714679,6.22476055 C9.63714679,6.04796303 9.49382408,5.90464032 9.31702657,5.90464032 Z M14.5798474,6.86256409 L14.4416086,6.86256409 L14.4416086,6.22476055 C14.4446698,6.10842468 14.3843547,5.99959157 14.2840842,5.94052196 C14.1838136,5.88145235 14.0593847,5.88145235 13.9591141,5.94052196 C13.8588436,5.99959157 13.7985285,6.10842468 13.8015897,6.22476055 L13.8015897,8.23609032 C13.7985285,8.35242618 13.8588436,8.46125929 13.9591141,8.5203289 C14.0593847,8.57939852 14.1838136,8.57939852 14.2840842,8.5203289 C14.3843547,8.46125929 14.4446698,8.35242618 14.4416086,8.23609032 L14.4416086,7.59828677 L14.5798474,7.59828677 C14.7830727,7.59828677 14.9478195,7.43354005 14.9478195,7.23031466 C14.9478195,7.02708928 14.7830727,6.86234256 14.5798474,6.86234256 L14.5798474,6.86256409 Z"
  }), wp.element.createElement("path", {
    d: "M8.72394881,10.2623647 C9.68591522,10.1939626 10.5942353,10.7122425 11.0247768,11.575198 C11.4562965,12.4374553 11.3255683,13.4746589 10.6937698,14.2029203 C10.0619713,14.9311817 9.05360159,15.206993 8.13909753,14.9016795 C6.89251576,14.4859636 6.21707177,13.1337553 6.63203317,11.8875508 C6.93613667,10.9723572 7.7619824,10.3307668 8.72394881,10.2623647 Z M8.97713093,10.6292747 C9.10746654,10.9061671 9.21328169,11.3969533 9.23629319,11.8136122 C9.10161935,11.8041813 8.96939756,11.8017292 8.84019367,11.8055016 C8.83095135,11.579159 8.75569244,11.3850703 8.61573728,11.2396452 C8.43994454,11.0570622 8.17380339,10.9553966 7.82466996,10.9321965 C7.7028222,11.0089644 7.58946229,11.1096868 7.48515608,11.1998466 C7.9523649,11.1745717 8.25528672,11.2507737 8.42391194,11.4246802 C8.5197303,11.5248368 8.56971429,11.6576245 8.57480699,11.8207797 C7.89087516,11.8817036 7.30936335,12.0938998 6.9149614,12.2793121 C6.89473528,12.3832606 6.88281466,12.4886541 6.87931244,12.5944941 C7.23938577,12.4094591 7.83542123,12.1618026 8.55594511,12.090316 C8.53029295,12.346649 8.456543,12.5497915 8.07119475,12.9174095 C7.68584651,13.2850276 7.4602584,13.5085409 7.31954876,13.8938892 C7.38293463,13.9737287 7.45229445,14.0486373 7.52702946,14.1179683 C7.64227556,13.7028183 7.72979469,13.6507595 8.25622981,13.110178 C8.78266493,12.5695964 8.79360482,12.4158721 8.82717897,12.0740948 C8.96505251,12.0696019 9.10306775,12.0724982 9.24063142,12.0827713 C9.22761672,12.9096762 9.11840643,13.5311752 8.78473974,14.651571 C8.87788357,14.6591239 8.97152837,14.6579873 9.06446145,14.6481758 C9.38115244,13.5709738 9.4930034,12.923634 9.50771567,12.1088007 C9.64484155,12.125965 9.76951859,12.1463358 9.8849533,12.1708563 C9.94248204,12.5160287 10.0709315,12.7778316 10.2065484,13.0526492 C10.32104,13.2850276 10.4389268,13.5260825 10.5175808,13.8335311 C10.5885738,13.7358091 10.6507479,13.6319752 10.7033704,13.5232532 C10.6271683,13.3027578 10.5358768,13.1160252 10.4462829,12.9341966 C10.336884,12.7123809 10.232955,12.4996189 10.1729742,12.2461152 C10.4696716,12.3391043 10.6911101,12.4718919 10.9085876,12.6201463 C10.9085876,12.6201463 10.8963274,12.3945582 10.8759566,12.2827072 C10.6444723,12.1352126 10.3917417,12.0240824 10.126574,11.9531902 C10.1095983,11.6695075 10.2025874,11.5150286 10.4649562,11.3829955 C10.4075667,11.3114449 10.3452998,11.2439471 10.2786008,11.1809847 C9.9902026,11.3448945 9.85703772,11.5655785 9.85703772,11.891889 C9.7476388,11.8713296 9.63107237,11.853788 9.505075,11.8385099 C9.48809931,11.4444852 9.39944847,10.9887821 9.28061861,10.6654895 C9.18052529,10.6457108 9.0790678,10.633604 8.97713093,10.6292747 Z"
  }))
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-heading/edit.jsx":
/*!*********************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-heading/edit.jsx ***!
  \*********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
/* harmony import */ var _components_category__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/category */ "./virtualgames-gutenberg/src/blocks/components/category.jsx");
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// External dependencies
 // WordPress dependencies

var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$blockEditor = wp.blockEditor,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;
var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner;

var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select; // Other Dependencies
// Internal dependencies




function ActivityHeading(_ref) {
  var isRequesting = _ref.isRequesting,
      activityCategories = _ref.activityCategories,
      setAttributes = _ref.setAttributes,
      category = _ref.attributes.category,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      className = _ref.className;

  var heading = function heading(categories, categoryAttribute) {
    var heading = categories && categories.map(function (category, index) {
      if (category.slug === categoryAttribute) {
        return category.jg_sport_tax_heading.content;
      }
    });
    heading = (0,_components_category__WEBPACK_IMPORTED_MODULE_1__.categoryResults)(categoryAttribute, heading, false);
    return wp.element.createElement("div", {
      className: "wp-block-cover__inner-container row",
      dangerouslySetInnerHTML: {
        __html: heading
      }
    });
  };

  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, (0,_components_category__WEBPACK_IMPORTED_MODULE_1__.categoryPanel)(setAttributes, category, activityCategories), wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  })));

  if (isRequesting) {
    return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_2__["default"],
      label: __('Activity Heading')
    }, wp.element.createElement(Spinner, null)));
  }

  var style = {
    backgroundColor: backgroundColor && backgroundColor.color
  };
  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('wp-block-cover', 'sport-heading', className, _defineProperty({}, backgroundColor["class"], backgroundColor["class"]));
  var blockProps = useBlockProps({
    className: classes,
    style: style
  });
  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement("div", blockProps, heading(activityCategories, category)));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select) {
  var _select = select('core'),
      getEntityRecords = _select.getEntityRecords;

  var _select2 = select('core/data'),
      isResolving = _select2.isResolving;

  var query = {
    per_page: -1
  };
  return {
    activityCategories: getEntityRecords('taxonomy', 'sport_tax', query),
    isRequesting: isResolving('core', 'getEntityRecords', ['taxonomy', 'sport_tax', query])
  };
}), withColors('backgroundColor', 'textColor'))(ActivityHeading));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-heading/index.jsx":
/*!**********************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-heading/index.jsx ***!
  \**********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/activity-heading/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/activity-heading/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
// Internal dependencies



var settings = {
  icon: wp.element.createElement("svg", {
    viewBox: "0 0 16 16",
    xmlns: "http://www.w3.org/2000/svg"
  }, wp.element.createElement("path", {
    d: "M10.029801,10.9162684 L7.46584185,10.9162684 L7.46584185,10.8857451 L7.19113195,10.9467917 C6.48909553,11.068885 5.97019905,11.6793514 5.97019905,12.4119112 C5.97019905,13.1444709 6.48909553,13.7549374 7.19113195,13.8770307 L7.40479521,13.907554 L7.40479521,15.8 L7.86264505,15.8 L7.86264505,11.3741182 L8.7478214,11.3741182 L8.7478214,15.8 L9.20567124,15.8 L9.20567124,11.3741182 L10.029801,11.3741182 L10.029801,10.9162684 Z",
    fill: "#000000"
  }), wp.element.createElement("rect", {
    fill: "#000000",
    x: "0",
    y: "0",
    width: "16",
    height: "8.7"
  }), wp.element.createElement("path", {
    d: "M8.85340895,3.42311997 C8.91645801,3.42311997 8.96756937,3.3720083 8.96756937,3.30895924 C8.96903751,3.0496131 9.16680467,2.83358339 9.42501312,2.80927346 C9.487778,2.80361626 9.53415459,2.74827513 9.52874671,2.68548827 C9.52240291,2.62316025 9.46772673,2.57712338 9.40522888,2.58148732 C9.0292848,2.6169068 8.7412984,2.93135744 8.73898117,3.30895924 C8.73898117,3.33928284 8.75104536,3.36836058 8.77251245,3.38977752 C8.79397954,3.41119445 8.82308542,3.42319067 8.85340895,3.42311997 L8.85340895,3.42311997 Z",
    fill: "#FFFFFF"
  }), wp.element.createElement("path", {
    d: "M9.44747091,4.58023044 C10.1612825,4.57978754 10.7396235,4.00084816 10.7393284,3.28703649 C10.7390331,2.57322482 10.1602134,1.99476403 9.44640165,1.99491154 C8.73258994,1.99505911 8.15400927,2.57375914 8.15400927,3.28757087 C8.15459945,4.00155561 8.73348593,4.58008314 9.44747091,4.58023044 L9.44747091,4.58023044 Z M8.4890046,3.09266935 C8.59130604,2.5884815 9.06691636,2.24989419 9.57681604,2.31825565 C10.0867157,2.38661712 10.4563456,2.83852449 10.4222301,3.35185392 C10.3881145,3.86518335 9.96193266,4.26420158 9.44747091,4.26449212 C9.15371848,4.26563444 8.87511195,4.13425031 8.68912929,3.906869 C8.50314663,3.67948769 8.42962298,3.38035952 8.4890046,3.09266935 L8.4890046,3.09266935 Z",
    fill: "#FFFFFF"
  }), wp.element.createElement("path", {
    d: "M10.6329213,4.94062446 C10.3005995,4.95506161 9.98966607,5.31812918 10.1276211,5.66408606 C10.3046098,6.10869679 9.83540246,6.30894539 9.50281331,6.01993505 C9.17022417,5.73092471 7.0380711,3.75303526 7.0380711,3.75303526 C7.0380711,3.75303526 6.8936996,3.61588234 6.67607295,3.40681103 C6.4996189,3.23971439 6.0536714,2.81381849 5.85208602,2.62292729 C5.65397625,2.43577906 5.21310848,2.02084469 5.04734862,1.86711579 C4.96366662,1.79118708 4.64631669,1.50110732 4.59926228,1.46341032 C4.20063655,1.1294844 3.75762995,1.59655292 4.15652304,2.04116364 C4.20999396,2.10185314 4.58054746,2.49352766 4.75994241,2.68281472 C4.91768163,2.84964401 5.34170605,3.29372002 5.54141995,3.50225663 C5.72028019,3.68940486 6.15125583,4.13668913 6.3464247,4.33961129 C7.70244732,5.74643128 8.22138263,6.25092944 8.7205337,6.70623436 C9.7803274,7.67298865 10.7791643,7.22597173 11.1411624,6.48593415 C11.4553041,5.84401572 11.1636202,4.91843403 10.6329213,4.94062446 Z",
    fill: "#FFFFFF"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-info/edit.jsx":
/*!******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-info/edit.jsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
/* harmony import */ var _components_category__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/category */ "./virtualgames-gutenberg/src/blocks/components/category.jsx");
/* harmony import */ var _components_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/forms */ "./virtualgames-gutenberg/src/blocks/components/forms.jsx");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// External dependencies
 // WordPress dependencies

var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$blockEditor = wp.blockEditor,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;
var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner,
    SelectControl = _wp$components.SelectControl,
    PanelBody = _wp$components.PanelBody;
var _wp = wp,
    apiFetch = _wp.apiFetch;

var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select,
    useSelect = _wp$data.useSelect;
var useState = wp.element.useState; // Other Dependencies
// Internal dependencies





function ActivityInfo(_ref) {
  var isResolvingRequest = _ref.isResolvingRequest,
      activityCategories = _ref.activityCategories,
      setAttributes = _ref.setAttributes,
      _ref$attributes = _ref.attributes,
      category = _ref$attributes.category,
      nfPost = _ref$attributes.nfPost,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      className = _ref.className,
      formSelectOptions = _ref.formSelectOptions,
      formLoaded = _ref.formLoaded;

  var info = function info(categories, categoryAttribute) {
    var info = categories && categories.map(function (category, index) {
      if (category.slug === categoryAttribute) {
        return category.jg_sport_tax_info.content;
      }
    });
    info = (0,_components_category__WEBPACK_IMPORTED_MODULE_1__.categoryResults)(categoryAttribute, info, false);
    return wp.element.createElement("div", {
      className: "wp-block-cover__inner-container row",
      dangerouslySetInnerHTML: {
        __html: info
      }
    });
  };

  var inspectorControls = wp.element.createElement(React.Fragment, null, (0,_components_category__WEBPACK_IMPORTED_MODULE_1__.categoryPanel)(setAttributes, category, activityCategories), wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  }));

  if (formLoaded) {
    inspectorControls = wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(PanelBody, {
      title: __('Ninja Forms'),
      initialOpen: false
    }, wp.element.createElement(SelectControl, {
      label: __('Add Activity'),
      value: nfPost,
      onChange: function onChange(value) {
        return setAttributes({
          nfPost: value
        });
      },
      options: formSelectOptions
    })));
  }

  var style = {
    backgroundColor: backgroundColor && backgroundColor.color
  };
  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('sport-info', 'wp-block-cover', className, _defineProperty({}, backgroundColor["class"], backgroundColor["class"]));
  var blockProps = useBlockProps({
    className: classes,
    style: style
  });

  if (isResolvingRequest) {
    return wp.element.createElement(React.Fragment, null, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_3__["default"],
      label: __('Activity Info')
    }, wp.element.createElement(Spinner, null)));
  }

  return wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, inspectorControls), wp.element.createElement("div", blockProps, info(activityCategories, category)));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select, props) {
  var _select = select('core'),
      getEntityRecords = _select.getEntityRecords;

  var _select2 = select('core/data'),
      isResolving = _select2.isResolving;

  var query = {
    per_page: -1,
    form_id: props.attributes.nfPost
  };
  var activityCategories = getEntityRecords('taxonomy', 'sport_tax', query);
  var isResolvingRequest = isResolving('core', 'getEntityRecords', ['taxonomy', 'sport_tax', query]);

  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      formSelectOptions = _useState2[0],
      setForms = _useState2[1];

  var _useState3 = useState(false),
      _useState4 = _slicedToArray(_useState3, 2),
      formLoaded = _useState4[0],
      setFormsformLoaded = _useState4[1];

  if (!formLoaded) {
    (0,_components_forms__WEBPACK_IMPORTED_MODULE_2__.getNinjaForms)({
      setFormsformLoaded: setFormsformLoaded,
      setForms: setForms
    });
  }

  return {
    activityCategories: activityCategories,
    isResolvingRequest: isResolvingRequest,
    formSelectOptions: formSelectOptions,
    formLoaded: formLoaded
  };
}), withColors('backgroundColor', 'textColor'))(ActivityInfo));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-info/index.jsx":
/*!*******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-info/index.jsx ***!
  \*******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/activity-info/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/activity-info/edit.jsx");
// External dependencies
// WordPress dependencies



var settings = {
  icon: wp.element.createElement("svg", {
    viewBox: "0 0 17 18",
    xmlns: "http://www.w3.org/2000/svg"
  }, wp.element.createElement("rect", {
    x: "0",
    y: "11",
    width: "17",
    height: "1",
    rx: "0.329089"
  }), wp.element.createElement("path", {
    d: "M7.41949595,14.2449099 L8.21101387,14.4111508 L8.12455879,15.4180727 L7.34666051,15.6913237 L6.86207784,14.9130323 L7.41949595,14.2449099 Z M8.71197958,15.7521339 L8.70704493,16.5517484 L9.56093683,16.8281584 L10.0879575,15.9547029 L9.48316674,15.3596318 L8.71197958,15.7521339 Z M10.8711852,16.2881718 C10.552349,17.2481758 9.68653287,17.9213646 8.67777776,17.9935885 C7.66902266,18.0658125 6.71617629,17.5228347 6.26389307,16.6180407 C5.81160985,15.7132466 5.94904471,14.6249911 6.61206241,13.8611183 C7.27508012,13.0972455 8.33298537,12.8083316 9.29209707,13.1292009 C9.92071459,13.3380678 10.4404505,13.7884732 10.7366813,14.3810838 C11.0329121,14.9736945 11.0813042,15.6598311 10.8711852,16.2881718 L10.8711852,16.2881718 Z M10.2867252,14.3390867 L9.81477524,14.6190505 L9.05148352,13.9696846 L9.23031525,13.4984056 C8.31406915,13.1633348 7.28739901,13.4946096 6.73962821,14.3020736 C6.19185741,15.1095375 6.26340467,16.1862006 6.91320082,16.9140429 L7.22428118,16.648097 L8.01599649,17.1464247 L7.90901327,17.5426781 C9.25005385,17.9073418 10.232444,17.0435607 10.5229963,16.1716847 C10.7276271,15.5567298 10.6406356,14.8819944 10.2867252,14.3390867 L10.2867252,14.3390867 Z"
  }), wp.element.createElement("path", {
    d: "M8.85015623,7.73367123 L8.84662544,7.85136424 C8.84293414,7.95569964 8.82294631,8.05881567 8.78738663,8.15697375 C8.7407267,8.27954402 8.68322135,8.3977057 8.61555483,8.51005277 C8.56992695,8.58745868 8.52892413,8.66749932 8.4927618,8.7497542 C8.33482509,9.15370416 8.45597738,9.61341687 8.79248666,9.88706096 C8.88852068,9.96327358 8.91872352,10.0919799 8.87416491,10.2002113 L8.83838693,10.2621093 L8.78721766,10.3116308 C8.69336116,10.3807319 8.56153652,10.3820717 8.4653001,10.3068327 C7.95058934,9.90667646 7.74972661,9.15030273 7.99884348,8.54732222 L8.06610053,8.40320608 L8.06610053,8.40320608 L8.14242895,8.26368208 L8.16361369,8.22445107 C8.21072579,8.14499767 8.25152086,8.06196462 8.2856221,7.97611883 L8.30449195,7.90940822 L8.30449195,7.90940822 L8.31426074,7.84077187 L8.32014539,7.73367123 L8.85015623,7.73367123 Z M7.72422646,7.42100014 L8.09731329,6.6093107 C8.24923006,6.65930673 8.40815315,6.68473443 8.56808532,6.68463423 C8.67298074,6.68473392 8.7774458,6.67376251 8.87976011,6.6519924 L9.03140346,6.61127225 L9.40645184,7.42100014 L7.72422646,7.42100014 L8.09731329,6.6093107 Z M8.52493122,3.17016557e-05 C9.91213946,-0.00621323219 11.0345384,0.910222984 11.0271215,2.71798758 C11.0247307,3.28840635 10.7563906,4.55282156 10.2252029,5.3135107 C9.81680813,5.89805263 9.28091264,6.37745548 8.56769301,6.37745548 C7.8862505,6.37745548 7.36016276,5.93571439 6.94862954,5.39236501 C6.34133362,4.59087563 5.99767004,3.32449887 6.00001189,2.7383877 C6.0078701,0.930230795 7.13654604,0.00555606854 8.52493122,3.17016557e-05 Z M7.6091088,0.667911565 C7.56429989,0.653871441 7.51564785,0.659377112 7.47510959,0.68307548 C6.80347483,1.02634675 6.23187913,2.2256385 6.50296535,3.25976772 C6.5908428,2.00594488 7.20951571,1.25780967 7.61084886,1.00123891 C7.69872631,0.944746266 7.74737275,0.8443149 7.71049561,0.756829765 C7.69072893,0.71423585 7.6539177,0.681951688 7.6091088,0.667911565 Z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-leaderboard/edit.jsx":
/*!*************************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-leaderboard/edit.jsx ***!
  \*************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

// External dependencies
 // WordPress dependencies

var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$blockEditor = wp.blockEditor,
    InnerBlocks = _wp$blockEditor.InnerBlocks,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;


var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner,
    PanelBody = _wp$components.PanelBody,
    SelectControl = _wp$components.SelectControl,
    RangeControl = _wp$components.RangeControl;
var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select,
    useSelect = _wp$data.useSelect;
var _wp = wp,
    apiFetch = _wp.apiFetch;
var useState = wp.element.useState; // Other Dependencies
// Internal dependencies

function ActivityLeaderboard(_ref) {
  var leaderboardLoaded = _ref.leaderboardLoaded,
      leaderboard = _ref.leaderboard,
      setAttributes = _ref.setAttributes,
      _ref$attributes = _ref.attributes,
      ages = _ref$attributes.ages,
      limit = _ref$attributes.limit,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      className = _ref.className;
  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  }), wp.element.createElement(PanelBody, {
    title: __('Content')
  }, wp.element.createElement(SelectControl, {
    label: __('Age Group'),
    value: ages,
    options: [{
      value: false,
      label: '--'
    }, {
      value: '0-12',
      label: '0-12'
    }, {
      value: '13-19',
      label: '13-19'
    }, {
      value: '20+',
      label: '20+'
    }],
    multiple: true,
    onChange: function onChange(value) {
      return setAttributes({
        ages: value
      });
    }
  }), wp.element.createElement(RangeControl, {
    label: __('Max number of participants'),
    value: limit,
    onChange: function onChange(value) {
      return setAttributes({
        limit: value
      });
    },
    min: 1,
    max: 15,
    type: "stepper"
  }))));

  if (!leaderboardLoaded) {
    return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__["default"],
      label: __('Leaderboard')
    }, wp.element.createElement(Spinner, null)));
  }

  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('aligncenter', className, {});
  var blockProps = useBlockProps({
    className: classes
  });

  if (!ages || ages === 'false') {
    return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement("h1", blockProps, "Select an age range"));
  } //console.log(leaderboard)


  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement("div", _extends({}, blockProps, {
    dangerouslySetInnerHTML: {
      __html: leaderboard
    }
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select, props) {
  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      leaderboard = _useState2[0],
      setLeaderboard = _useState2[1];

  var _useState3 = useState(false),
      _useState4 = _slicedToArray(_useState3, 2),
      leaderboardLoaded = _useState4[0],
      setLeaderboardLoaded = _useState4[1]; //if(!leaderboardLoaded) {


  apiFetch({
    path: "/jensen/v1/sports/leaderboard",
    method: 'POST',
    data: {
      attributes: props.attributes
    }
  }).then(function (result) {
    if (!result) {
      console.log('Error');
      console.log(result);
    }

    setLeaderboard(result);
    setLeaderboardLoaded(true); //return {
    //    formSelectOptions: setForms( parsed ),
    //    formLoaded: setFormsformLoaded( true )
    //}
  })["catch"](function () {
    setLeaderboard([]);
    setLeaderboardLoaded(true); //return {
    //    formSelectOptions: setForms( [] ),
    //    formLoaded: setFormsformLoaded( true )
    //}
  }); //}

  return {
    leaderboard: leaderboard,
    leaderboardLoaded: leaderboardLoaded
  };
}), withColors('backgroundColor', 'overlayColor', 'textColor'))(ActivityLeaderboard));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-leaderboard/index.jsx":
/*!**************************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-leaderboard/index.jsx ***!
  \**************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/activity-leaderboard/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/activity-leaderboard/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
// Internal dependencies


var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/blocks.jsx":
/*!******************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/blocks.jsx ***!
  \******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _activity_heading__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./activity-heading */ "./virtualgames-gutenberg/src/blocks/activity-heading/index.jsx");
/* harmony import */ var _activity_info__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./activity-info */ "./virtualgames-gutenberg/src/blocks/activity-info/index.jsx");
/* harmony import */ var _activity_categories__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./activity-categories */ "./virtualgames-gutenberg/src/blocks/activity-categories/index.jsx");
/* harmony import */ var _activity_leaderboard__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./activity-leaderboard */ "./virtualgames-gutenberg/src/blocks/activity-leaderboard/index.jsx");
/* harmony import */ var _page_heading__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./page-heading */ "./virtualgames-gutenberg/src/blocks/page-heading/index.jsx");
/* harmony import */ var _guestbook__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./guestbook */ "./virtualgames-gutenberg/src/blocks/guestbook/index.jsx");
/* harmony import */ var _user_set_user_data__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./user-set-user-data */ "./virtualgames-gutenberg/src/blocks/user-set-user-data/index.jsx");
/* harmony import */ var _user_register__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./user-register */ "./virtualgames-gutenberg/src/blocks/user-register/index.jsx");
/* harmony import */ var _user_profile__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./user-profile */ "./virtualgames-gutenberg/src/blocks/user-profile/index.jsx");
/* harmony import */ var _user_sign_in__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ./user-sign-in */ "./virtualgames-gutenberg/src/blocks/user-sign-in/index.jsx");
// Blocks










var registerBlockType = wp.blocks.registerBlockType; // Register blocks

[_activity_heading__WEBPACK_IMPORTED_MODULE_0__["default"], _activity_info__WEBPACK_IMPORTED_MODULE_1__["default"], _activity_categories__WEBPACK_IMPORTED_MODULE_2__["default"], _activity_leaderboard__WEBPACK_IMPORTED_MODULE_3__["default"], _page_heading__WEBPACK_IMPORTED_MODULE_4__["default"], _guestbook__WEBPACK_IMPORTED_MODULE_5__["default"], _user_sign_in__WEBPACK_IMPORTED_MODULE_9__["default"], _user_set_user_data__WEBPACK_IMPORTED_MODULE_6__["default"], _user_register__WEBPACK_IMPORTED_MODULE_7__["default"], _user_profile__WEBPACK_IMPORTED_MODULE_8__["default"]].forEach(function (_ref) {
  var metadata = _ref.metadata,
      settings = _ref.settings;
  //console.log(metadata.title)
  registerBlockType(metadata, settings);
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/components/category.jsx":
/*!*******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/components/category.jsx ***!
  \*******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "categoryPanel": function() { return /* binding */ categoryPanel; },
/* harmony export */   "categoryResults": function() { return /* binding */ categoryResults; },
/* harmony export */   "multipleCategoriesPanel": function() { return /* binding */ multipleCategoriesPanel; }
/* harmony export */ });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

 // WordPress dependencies


var __ = wp.i18n.__;
var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner,
    PanelBody = _wp$components.PanelBody,
    SelectControl = _wp$components.SelectControl;
function categoryPanel(setAttributes, category, sportCategories) {
  var options = sportCategories ? sportCategories.map(function (signleCategory) {
    return {
      value: signleCategory.slug,
      label: signleCategory.name
    };
  }) : [];
  return wp.element.createElement(PanelBody, {
    title: __('Content')
  }, wp.element.createElement(SelectControl, {
    label: __('Activity Category'),
    value: category,
    options: [{
      value: false,
      label: '--'
    }].concat(_toConsumableArray(options)),
    onChange: function onChange(value) {
      return setAttributes({
        category: value
      });
    }
  }));
}
function multipleCategoriesPanel(setAttributes, categories, sportCategories) {
  var options = sportCategories ? sportCategories.map(function (signleCategory) {
    return {
      value: signleCategory.slug,
      label: signleCategory.name
    };
  }) : [];
  return wp.element.createElement(PanelBody, {
    title: __('Content', 'text-domain')
  }, wp.element.createElement(SelectControl, {
    label: __('Select Categories'),
    value: categories,
    options: [{
      value: false,
      label: '--'
    }].concat(_toConsumableArray(options)),
    multiple: true,
    onChange: function onChange(value) {
      return setAttributes({
        categories: value
      });
    }
  }));
}
function categoryResults(categoryAttribute, posts) {
  var requireActivities = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : true;

  if (posts) {
    posts = posts.filter(function (item) {
      return item;
    });
  }

  if (!categoryAttribute || categoryAttribute === 'false') {
    posts = "Please select category from block settings.";
  } else if (requireActivities && posts && posts.length == 0) {
    posts = "No sports are available yet in the ".concat(categoryAttribute, " category.");
  }

  return posts;
}

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/components/forms.jsx":
/*!****************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/components/forms.jsx ***!
  \****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony export */ __webpack_require__.d(__webpack_exports__, {
/* harmony export */   "getFormName": function() { return /* binding */ getFormName; },
/* harmony export */   "getNinjaForms": function() { return /* binding */ getNinjaForms; }
/* harmony export */ });
function _toConsumableArray(arr) { return _arrayWithoutHoles(arr) || _iterableToArray(arr) || _unsupportedIterableToArray(arr) || _nonIterableSpread(); }

function _nonIterableSpread() { throw new TypeError("Invalid attempt to spread non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _iterableToArray(iter) { if (typeof Symbol !== "undefined" && iter[Symbol.iterator] != null || iter["@@iterator"] != null) return Array.from(iter); }

function _arrayWithoutHoles(arr) { if (Array.isArray(arr)) return _arrayLikeToArray(arr); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

var _wp = wp,
    apiFetch = _wp.apiFetch;
var getNinjaForms = function getNinjaForms(_ref) {
  var setFormsformLoaded = _ref.setFormsformLoaded,
      setForms = _ref.setForms;
  apiFetch({
    path: "/jensen/v1/ninja/forms"
  }).then(function (result) {
    if (!result) {
      console.log('Error');
      console.log(result);
    }

    var parsed = JSON.parse(result);
    parsed = [{
      label: 'Choose Form',
      value: false
    }].concat(_toConsumableArray(parsed));
    setForms(parsed);
    setFormsformLoaded(true);
  })["catch"](function () {
    setForms([]);
    setFormsformLoaded(true);
  });
};
var getFormName = function getFormName(key, formSelectOptions) {
  var label = 'Form not found';
  formSelectOptions && formSelectOptions.map(function (form, index) {
    var formKey = '' + form.value;

    if (formKey === key) {
      label = form.label;
    }
  });
  return label;
};

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/guestbook/edit.jsx":
/*!**************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/guestbook/edit.jsx ***!
  \**************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
/* harmony import */ var _components_forms__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../components/forms */ "./virtualgames-gutenberg/src/blocks/components/forms.jsx");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

function _extends() { _extends = Object.assign || function (target) { for (var i = 1; i < arguments.length; i++) { var source = arguments[i]; for (var key in source) { if (Object.prototype.hasOwnProperty.call(source, key)) { target[key] = source[key]; } } } return target; }; return _extends.apply(this, arguments); }

// External dependencies
 // WordPress dependencies

var __ = wp.i18n.__;
var _wp$blockEditor = wp.blockEditor,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps,
    Disabled = _wp$blockEditor.Disabled;

var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner,
    guestbookLoaded = _wp$components.guestbookLoaded,
    guestbook = _wp$components.guestbook,
    PanelBody = _wp$components.PanelBody,
    SelectControl = _wp$components.SelectControl;
var compose = wp.compose.compose;
var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select,
    useSelect = _wp$data.useSelect;
var _wp = wp,
    apiFetch = _wp.apiFetch;
var useState = wp.element.useState; // Other Dependencies
// Internal dependencies




function Guestbook(_ref) {
  var backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      className = _ref.className,
      nfComment = _ref.attributes.nfComment,
      setAttributes = _ref.setAttributes,
      guestbook = _ref.guestbook,
      guestbookLoaded = _ref.guestbookLoaded,
      formSelectOptions = _ref.formSelectOptions,
      formLoaded = _ref.formLoaded;
  var inspectorControls = wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  });

  if (formLoaded) {
    inspectorControls = wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(PanelBody, {
      title: __('Ninja Forms'),
      initialOpen: false
    }, wp.element.createElement(SelectControl, {
      label: __('Add guestbook form'),
      value: nfComment,
      onChange: function onChange(value) {
        return setAttributes({
          nfComment: value
        });
      },
      options: formSelectOptions
    })));
  }

  if (!guestbookLoaded) {
    return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_2__["default"],
      label: __('Guestbook')
    }, wp.element.createElement(Spinner, null)));
  }

  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('guestbook', className, {});
  var blockProps = useBlockProps({
    className: classes
  });
  return wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, inspectorControls), wp.element.createElement("div", _extends({}, blockProps, {
    dangerouslySetInnerHTML: {
      __html: guestbook
    }
  })));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select, props) {
  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      guestbook = _useState2[0],
      setGuestbook = _useState2[1];

  var _useState3 = useState(false),
      _useState4 = _slicedToArray(_useState3, 2),
      guestbookLoaded = _useState4[0],
      setGuestbookLoaded = _useState4[1];

  if (!guestbookLoaded) {
    apiFetch({
      path: "/jensen/v1/posts/guestbook"
    }).then(function (result) {
      if (!result) {
        console.log('Error');
        console.log(result);
      }

      setGuestbook(result);
      setGuestbookLoaded(true); //return {
      //    formSelectOptions: setForms( parsed ),
      //    formLoaded: setFormsformLoaded( true )
      //}
    })["catch"](function () {
      setGuestbook([]);
      setGuestbookLoaded(true); //return {
      //    formSelectOptions: setForms( [] ),
      //    formLoaded: setFormsformLoaded( true )
      //}
    });
  }

  var _useState5 = useState(null),
      _useState6 = _slicedToArray(_useState5, 2),
      formSelectOptions = _useState6[0],
      setForms = _useState6[1];

  var _useState7 = useState(false),
      _useState8 = _slicedToArray(_useState7, 2),
      formLoaded = _useState8[0],
      setFormsformLoaded = _useState8[1];

  if (!formLoaded) {
    (0,_components_forms__WEBPACK_IMPORTED_MODULE_1__.getNinjaForms)({
      setFormsformLoaded: setFormsformLoaded,
      setForms: setForms
    });
  }

  return {
    guestbook: guestbook,
    guestbookLoaded: guestbookLoaded,
    formSelectOptions: formSelectOptions,
    formLoaded: formLoaded
  };
}), withColors('backgroundColor', 'overlayColor', 'textColor'))(Guestbook));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/guestbook/index.jsx":
/*!***************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/guestbook/index.jsx ***!
  \***************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/guestbook/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/guestbook/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
// Internal dependencies


var settings = {
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/page-heading/edit.jsx":
/*!*****************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/page-heading/edit.jsx ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! classnames */ "./node_modules/classnames/index.js");
/* harmony import */ var classnames__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(classnames__WEBPACK_IMPORTED_MODULE_0__);
function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

// External dependencies
 // WordPress dependencies


var __ = wp.i18n.__;
var compose = wp.compose.compose;
var _wp$blockEditor = wp.blockEditor,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;
var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select; // Other Dependencies
// Internal dependencies

function PageHeading(_ref) {
  var _classnames;

  var backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      className = _ref.className,
      attributes = _ref.attributes,
      pageTitle = _ref.pageTitle;
  var inspectorControls = wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  })));
  var style = {
    backgroundColor: backgroundColor && backgroundColor.color
  };
  var classes = classnames__WEBPACK_IMPORTED_MODULE_0___default()('wp-block-cover', 'page-heading', 'has-background-dim', className, (_classnames = {}, _defineProperty(_classnames, backgroundColor["class"], backgroundColor["class"]), _defineProperty(_classnames, textColor["class"], textColor["class"]), _classnames));
  var blockProps = useBlockProps({
    className: classes,
    style: style
  });
  return wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement("div", blockProps, wp.element.createElement("div", {
    className: "wp-block-cover__inner-container"
  }, wp.element.createElement("h1", {
    className: "entry-title"
  }, pageTitle))));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select, props) {
  var pageTitle = select("core/editor").getEditedPostAttribute('title');
  return {
    pageTitle: pageTitle
  };
}), withColors('backgroundColor', 'textColor'))(PageHeading));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/page-heading/index.jsx":
/*!******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/page-heading/index.jsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/page-heading/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/page-heading/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
// Internal dependencies



var settings = {
  icon: wp.element.createElement("svg", {
    viewBox: "0 0 16 10",
    xmlns: "http://www.w3.org/2000/svg"
  }, wp.element.createElement("rect", {
    fill: "#000000",
    x: "0",
    y: "0.55",
    width: "16",
    height: "8.7"
  }), wp.element.createElement("path", {
    d: "M10.029801,2.47339585 L7.46584183,2.47339585 L7.46584183,2.44287255 L7.19113192,2.50391915 C6.4890955,2.62601245 5.97019902,3.23647885 5.97019902,3.96903865 C5.97019902,4.70159835 6.4890955,5.31206485 7.19113192,5.43415815 L7.40479518,5.46468145 L7.40479518,7.35712745 L7.86264502,7.35712745 L7.86264502,2.93124565 L8.74782137,2.93124565 L8.74782137,7.35712745 L9.20567121,7.35712745 L9.20567121,2.93124565 L10.029801,2.93124565 L10.029801,2.47339585 Z",
    fill: "#FFFFFF"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-profile/edit.jsx":
/*!*****************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-profile/edit.jsx ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @wordpress/i18n */ "./node_modules/@wordpress/i18n/build-module/index.js");
/* harmony import */ var _wordpress_api_fetch__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/api-fetch */ "./node_modules/@wordpress/api-fetch/build-module/index.js");
/* harmony import */ var _wordpress_compose__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @wordpress/compose */ "./node_modules/@wordpress/compose/build-module/higher-order/compose.js");
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
/* harmony import */ var _components_forms__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/forms */ "./virtualgames-gutenberg/src/blocks/components/forms.jsx");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// External dependencies
// WordPress dependencies

var _wp$blockEditor = wp.blockEditor,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;

var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner,
    SelectControl = _wp$components.SelectControl,
    PanelBody = _wp$components.PanelBody;

var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select,
    useSelect = _wp$data.useSelect;
var useState = wp.element.useState;
 // Other Dependencies
// Internal dependencies




function Profile(_ref) {
  var setAttributes = _ref.setAttributes,
      _ref$attributes = _ref.attributes,
      nfProfile = _ref$attributes.nfProfile,
      nfPassword = _ref$attributes.nfPassword,
      nfFamily = _ref$attributes.nfFamily,
      nfComment = _ref$attributes.nfComment,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      formSelectOptions = _ref.formSelectOptions,
      formLoaded = _ref.formLoaded;
  var inspectorControls = wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Background color')
    }]
  }));

  if (formLoaded) {
    inspectorControls = wp.element.createElement(React.Fragment, null, inspectorControls, wp.element.createElement(PanelBody, {
      title: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Ninja Forms'),
      initialOpen: false
    }, wp.element.createElement(SelectControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Select Profile Form', 'jensen'),
      value: nfProfile,
      onChange: function onChange(value) {
        return setAttributes({
          nfProfile: value
        });
      },
      options: formSelectOptions
    }), wp.element.createElement(SelectControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Select Password Form', 'jensen'),
      value: nfPassword,
      onChange: function onChange(value) {
        return setAttributes({
          nfPassword: value
        });
      },
      options: formSelectOptions
    }), wp.element.createElement(SelectControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Select Add Family Form', 'jensen'),
      value: nfFamily,
      onChange: function onChange(value) {
        return setAttributes({
          nfFamily: value
        });
      },
      options: formSelectOptions
    }), wp.element.createElement(SelectControl, {
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Select Add Comment Form', 'jensen'),
      value: nfComment,
      onChange: function onChange(value) {
        return setAttributes({
          nfComment: value
        });
      },
      options: formSelectOptions
    })));
  } else {
    return wp.element.createElement(React.Fragment, null, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_3__["default"],
      label: (0,_wordpress_i18n__WEBPACK_IMPORTED_MODULE_0__.__)('Profile Info')
    }, wp.element.createElement(Spinner, null)));
  }

  var profileFormLabel = (0,_components_forms__WEBPACK_IMPORTED_MODULE_2__.getFormName)(nfProfile, formSelectOptions);
  var passwordFormLabel = (0,_components_forms__WEBPACK_IMPORTED_MODULE_2__.getFormName)(nfPassword, formSelectOptions);
  var familyFormLabel = (0,_components_forms__WEBPACK_IMPORTED_MODULE_2__.getFormName)(nfFamily, formSelectOptions);
  var commentFormLabel = (0,_components_forms__WEBPACK_IMPORTED_MODULE_2__.getFormName)(nfComment, formSelectOptions);
  var blockProps = useBlockProps();
  return wp.element.createElement(React.Fragment, null, "  ", wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, inspectorControls), wp.element.createElement("div", blockProps, wp.element.createElement("h3", {
    className: "title"
  }, "Will display profile"), wp.element.createElement("div", null, "Profile Form - ", profileFormLabel), wp.element.createElement("div", null, "Password Form - ", passwordFormLabel), wp.element.createElement("div", null, "Family Entry Form - ", familyFormLabel), wp.element.createElement("div", null, "Guestbook Entry Form - ", commentFormLabel)));
}

/* harmony default export */ __webpack_exports__["default"] = ((0,_wordpress_compose__WEBPACK_IMPORTED_MODULE_4__["default"])(withSelect(function (select, props) {
  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      formSelectOptions = _useState2[0],
      setForms = _useState2[1];

  var _useState3 = useState(false),
      _useState4 = _slicedToArray(_useState3, 2),
      formLoaded = _useState4[0],
      setFormsformLoaded = _useState4[1];

  if (!formLoaded) {
    (0,_components_forms__WEBPACK_IMPORTED_MODULE_2__.getNinjaForms)({
      setFormsformLoaded: setFormsformLoaded,
      setForms: setForms
    });
  }

  return {
    formSelectOptions: formSelectOptions,
    formLoaded: formLoaded
  };
}), withColors('backgroundColor', 'overlayColor', 'textColor'))(Profile));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-profile/index.jsx":
/*!******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-profile/index.jsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/user-profile/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/user-profile/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
// Internal dependencies



var settings = {
  icon: wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 512 512"
  }, wp.element.createElement("path", {
    d: "M256 288c79.5 0 144-64.5 144-144S335.5 0 256 0 112 64.5 112 144s64.5 144 144 144zm128 32h-55.1c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16H128C57.3 320 0 377.3 0 448v16c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48v-16c0-70.7-57.3-128-128-128z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-register/edit.jsx":
/*!******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-register/edit.jsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
/* harmony import */ var _components_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/forms */ "./virtualgames-gutenberg/src/blocks/components/forms.jsx");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var useState = wp.element.useState;
var _wp$blockEditor = wp.blockEditor,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;
var _wp = wp,
    ServerSideRender = _wp.ServerSideRender;
var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner,
    PanelBody = _wp$components.PanelBody,
    SelectControl = _wp$components.SelectControl,
    Disabled = _wp$components.Disabled;
var compose = wp.compose.compose;
var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select;
 // Other Dependencies
// Internal dependencies




function FinishRegistration(_ref) {
  var setAttributes = _ref.setAttributes,
      nfRegistration = _ref.attributes.nfRegistration,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      formSelectOptions = _ref.formSelectOptions,
      formLoaded = _ref.formLoaded;
  var formsInspector;

  if (formLoaded) {
    formsInspector = wp.element.createElement(PanelBody, {
      title: __('Ninja Form'),
      initialOpen: false
    }, wp.element.createElement(SelectControl, {
      label: __('Registration'),
      value: nfRegistration,
      onChange: function onChange(value) {
        return setAttributes({
          nfRegistration: value
        });
      },
      options: formSelectOptions
    }));
  } else {
    return wp.element.createElement(React.Fragment, null, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__["default"],
      label: __('Activity Info')
    }, wp.element.createElement(Spinner, null)));
  }

  var registrationFormLabel = (0,_components_forms__WEBPACK_IMPORTED_MODULE_0__.getFormName)(nfRegistration, formSelectOptions);
  var blockProps = useBlockProps();
  return wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  }), formsInspector), wp.element.createElement("div", blockProps, wp.element.createElement("h3", {
    className: "title"
  }, "Will display Registration Form - ", registrationFormLabel)));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select, props) {
  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      formSelectOptions = _useState2[0],
      setForms = _useState2[1];

  var _useState3 = useState(false),
      _useState4 = _slicedToArray(_useState3, 2),
      formLoaded = _useState4[0],
      setFormsformLoaded = _useState4[1];

  if (!formLoaded) {
    (0,_components_forms__WEBPACK_IMPORTED_MODULE_0__.getNinjaForms)({
      setFormsformLoaded: setFormsformLoaded,
      setForms: setForms
    });
  }

  return {
    formSelectOptions: formSelectOptions,
    formLoaded: formLoaded
  };
}), withColors('backgroundColor', 'overlayColor', 'textColor'))(FinishRegistration));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-register/index.jsx":
/*!*******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-register/index.jsx ***!
  \*******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/user-register/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/user-register/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
// Internal dependencies



var name = 'jg/register';
var settings = {
  icon: wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 512 512"
  }, wp.element.createElement("path", {
    d: "M256 288c79.5 0 144-64.5 144-144S335.5 0 256 0 112 64.5 112 144s64.5 144 144 144zm128 32h-55.1c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16H128C57.3 320 0 377.3 0 448v16c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48v-16c0-70.7-57.3-128-128-128z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-set-user-data/edit.jsx":
/*!***********************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-set-user-data/edit.jsx ***!
  \***********************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @wordpress/icons */ "./node_modules/@wordpress/icons/build-module/library/pin.js");
/* harmony import */ var _components_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/forms */ "./virtualgames-gutenberg/src/blocks/components/forms.jsx");
function _slicedToArray(arr, i) { return _arrayWithHoles(arr) || _iterableToArrayLimit(arr, i) || _unsupportedIterableToArray(arr, i) || _nonIterableRest(); }

function _nonIterableRest() { throw new TypeError("Invalid attempt to destructure non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

function _iterableToArrayLimit(arr, i) { var _i = arr == null ? null : typeof Symbol !== "undefined" && arr[Symbol.iterator] || arr["@@iterator"]; if (_i == null) return; var _arr = []; var _n = true; var _d = false; var _s, _e; try { for (_i = _i.call(arr); !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"] != null) _i["return"](); } finally { if (_d) throw _e; } } return _arr; }

function _arrayWithHoles(arr) { if (Array.isArray(arr)) return arr; }

// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var useState = wp.element.useState;
var _wp$blockEditor = wp.blockEditor,
    BlockControls = _wp$blockEditor.BlockControls,
    BlockIcon = _wp$blockEditor.BlockIcon,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;
var _wp$components = wp.components,
    Placeholder = _wp$components.Placeholder,
    Spinner = _wp$components.Spinner,
    PanelBody = _wp$components.PanelBody,
    ToggleControl = _wp$components.ToggleControl,
    SelectControl = _wp$components.SelectControl;
var _wp$data = wp.data,
    withSelect = _wp$data.withSelect,
    select = _wp$data.select;

var compose = wp.compose.compose; // Other Dependencies
// Internal dependencies




function FinishRegistration(_ref) {
  var setAttributes = _ref.setAttributes,
      nfRegistration = _ref.attributes.nfRegistration,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      formSelectOptions = _ref.formSelectOptions,
      formLoaded = _ref.formLoaded;
  var formsInspector;

  if (formLoaded) {
    formsInspector = wp.element.createElement(PanelBody, {
      title: __('Ninja Form'),
      initialOpen: false
    }, wp.element.createElement(SelectControl, {
      label: __('Registration'),
      value: nfRegistration,
      onChange: function onChange(value) {
        return setAttributes({
          nfRegistration: value
        });
      },
      options: formSelectOptions
    }));
  } else {
    return wp.element.createElement(React.Fragment, null, wp.element.createElement(Placeholder, {
      icon: _wordpress_icons__WEBPACK_IMPORTED_MODULE_1__["default"],
      label: __('Activity Info')
    }, wp.element.createElement(Spinner, null)));
  }

  var registrationFormLabel = (0,_components_forms__WEBPACK_IMPORTED_MODULE_0__.getFormName)(nfRegistration, formSelectOptions);
  var blockProps = useBlockProps();
  return wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  }), formsInspector), wp.element.createElement("div", blockProps, wp.element.createElement("h3", {
    className: "title"
  }, "Will set profile info from Registration Form - ", registrationFormLabel)));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withSelect(function (select, props) {
  var _useState = useState(null),
      _useState2 = _slicedToArray(_useState, 2),
      formSelectOptions = _useState2[0],
      setForms = _useState2[1];

  var _useState3 = useState(false),
      _useState4 = _slicedToArray(_useState3, 2),
      formLoaded = _useState4[0],
      setFormsformLoaded = _useState4[1];

  if (!formLoaded) {
    (0,_components_forms__WEBPACK_IMPORTED_MODULE_0__.getNinjaForms)({
      setFormsformLoaded: setFormsformLoaded,
      setForms: setForms
    });
  }

  return {
    formSelectOptions: formSelectOptions,
    formLoaded: formLoaded
  };
}), withColors('backgroundColor', 'overlayColor', 'textColor'))(FinishRegistration));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-set-user-data/index.jsx":
/*!************************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-set-user-data/index.jsx ***!
  \************************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/user-set-user-data/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/user-set-user-data/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
var __ = wp.i18n.__; // Internal dependencies




var name = 'jg/set-user-data';
var settings = {
  icon: wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 512 512"
  }, wp.element.createElement("path", {
    d: "M256 288c79.5 0 144-64.5 144-144S335.5 0 256 0 112 64.5 112 144s64.5 144 144 144zm128 32h-55.1c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16H128C57.3 320 0 377.3 0 448v16c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48v-16c0-70.7-57.3-128-128-128z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-sign-in/edit.jsx":
/*!*****************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-sign-in/edit.jsx ***!
  \*****************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _components_forms__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../components/forms */ "./virtualgames-gutenberg/src/blocks/components/forms.jsx");
// External dependencies
// WordPress dependencies
var __ = wp.i18n.__;
var _wp$blockEditor = wp.blockEditor,
    InspectorControls = _wp$blockEditor.InspectorControls,
    withColors = _wp$blockEditor.withColors,
    PanelColorSettings = _wp$blockEditor.PanelColorSettings,
    useBlockProps = _wp$blockEditor.useBlockProps;
var PanelBody = wp.components.PanelBody;
var compose = wp.compose.compose;
 // Other Dependencies
// Internal dependencies




function Signin(_ref) {
  var setAttributes = _ref.setAttributes,
      attributes = _ref.attributes,
      _ref$attributes = _ref.attributes,
      bg = _ref$attributes.bg,
      primaryLogo = _ref$attributes.primaryLogo,
      secondaryLogo = _ref$attributes.secondaryLogo,
      nfSignin = _ref$attributes.nfSignin,
      backgroundColor = _ref.backgroundColor,
      setBackgroundColor = _ref.setBackgroundColor,
      textColor = _ref.textColor,
      setTextColor = _ref.setTextColor,
      formSelectOptions = _ref.formSelectOptions,
      formLoaded = _ref.formLoaded;
  var blockProps = useBlockProps();
  return wp.element.createElement(React.Fragment, null, wp.element.createElement(InspectorControls, {
    key: "inspector"
  }, wp.element.createElement(PanelColorSettings, {
    title: __('Background Color'),
    initialOpen: true,
    colorSettings: [{
      value: textColor.color,
      onChange: setTextColor,
      label: __('Text color')
    }, {
      value: backgroundColor.color,
      onChange: setBackgroundColor,
      label: __('Background color')
    }]
  })), wp.element.createElement("div", blockProps, wp.element.createElement("h3", {
    className: "title"
  }, "Will display form: ", nfSignin)));
}

/* harmony default export */ __webpack_exports__["default"] = (compose(withColors('backgroundColor', 'overlayColor', 'textColor'))(Signin));

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-sign-in/index.jsx":
/*!******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-sign-in/index.jsx ***!
  \******************************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _block_json__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./block.json */ "./virtualgames-gutenberg/src/blocks/user-sign-in/block.json");
/* harmony import */ var _edit__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./edit */ "./virtualgames-gutenberg/src/blocks/user-sign-in/edit.jsx");
// External dependencies
//import classnames from 'classnames/dedupe'
// WordPress dependencies
var __ = wp.i18n.__; // Internal dependencies




var name = 'jg/signin';
var settings = {
  icon: wp.element.createElement("svg", {
    xmlns: "http://www.w3.org/2000/svg",
    viewBox: "0 0 512 512"
  }, wp.element.createElement("path", {
    d: "M256 288c79.5 0 144-64.5 144-144S335.5 0 256 0 112 64.5 112 144s64.5 144 144 144zm128 32h-55.1c-22.2 10.2-46.9 16-72.9 16s-50.6-5.8-72.9-16H128C57.3 320 0 377.3 0 448v16c0 26.5 21.5 48 48 48h416c26.5 0 48-21.5 48-48v-16c0-70.7-57.3-128-128-128z"
  })),
  edit: _edit__WEBPACK_IMPORTED_MODULE_1__["default"]
};
/* harmony default export */ __webpack_exports__["default"] = ({
  metadata: _block_json__WEBPACK_IMPORTED_MODULE_0__,
  settings: settings
});

/***/ }),

/***/ "./virtualgames-gutenberg/src/styles/app.scss":
/*!****************************************************!*\
  !*** ./virtualgames-gutenberg/src/styles/app.scss ***!
  \****************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/editor.scss":
/*!*******************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/editor.scss ***!
  \*******************************************************/
/***/ (function(__unused_webpack_module, __webpack_exports__, __webpack_require__) {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ }),

/***/ "lodash":
/*!*************************!*\
  !*** external "lodash" ***!
  \*************************/
/***/ (function(module) {

module.exports = lodash;

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-categories/block.json":
/*!**************************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-categories/block.json ***!
  \**************************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/sport-categories","category":"common","icon":"","title":"Activities Categories","description":"Show links to all activity categories","keywords":["jensen","wa","virtualgames"],"attributes":{"categories":{"type":"array","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-heading/block.json":
/*!***********************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-heading/block.json ***!
  \***********************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/sport-heading","category":"common","title":"Activity Intro","description":"Intro image and text for a activity category","keywords":["jensen","wa","virtualgames"],"attributes":{"backgroundColor":{"type":"string","default":"white"},"textColor":{"type":"string","default":"black"},"category":{"type":"string","default":false},"align":{"type":"string","default":"full"}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-info/block.json":
/*!********************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-info/block.json ***!
  \********************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/sport-info","category":"common","title":"Activity Info","description":"Aggregates sport posts by category","keywords":["jensen","wa","virtualgames"],"attributes":{"backgroundColor":{"type":"string","default":"white"},"textColor":{"type":"string","default":"black"},"category":{"type":"string","default":false},"nfPost":{"type":"string","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/activity-leaderboard/block.json":
/*!***************************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/activity-leaderboard/block.json ***!
  \***************************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/sport-leaderboard","category":"common","icon":"shield-alt","title":"Activities Leaderboard","description":"Show show current rankings of participants by age category","keywords":["jensen","wa","virtualgames"],"attributes":{"backgroundColor":{"type":"string","default":"white"},"textColor":{"type":"string","default":"black"},"ages":{"type":"array","default":false},"limit":{"type":"number","default":10},"align":{"type":"string","default":"full"}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/guestbook/block.json":
/*!****************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/guestbook/block.json ***!
  \****************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/guestbook","category":"common","icon":"universal-access","title":"Guestbook","description":"Show guestbook entries","keywords":["jensen","wa","virtualgames"],"attributes":{"backgroundColor":{"type":"string","default":"white"},"textColor":{"type":"string","default":"black"},"nfComment":{"type":"string","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/page-heading/block.json":
/*!*******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/page-heading/block.json ***!
  \*******************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/page-heading","category":"common","icon":"","title":"Page Intro","description":"Intro block with page title","keywords":["jensen","wa","virtualgames"],"attributes":{"category":{"type":"string","default":""},"backgroundColor":{"type":"string","default":"secondary"},"textColor":{"type":"string","default":"white"},"align":{"type":"string","default":"full"}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-profile/block.json":
/*!*******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-profile/block.json ***!
  \*******************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/profile","category":"common","icon":"","title":"Profile","description":"Allow users to edit their profile","keywords":["jensen","wa","virtualgames"],"attributes":{"backgroundColor":{"type":"string","default":"secondary"},"textColor":{"type":"string","default":"black"},"category":{"type":"string","default":false},"nfProfile":{"type":"string","default":false},"nfPassword":{"type":"string","default":false},"nfFamily":{"type":"string","default":false},"nfPost":{"type":"string","default":false},"nfComment":{"type":"string","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-register/block.json":
/*!********************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-register/block.json ***!
  \********************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/register","category":"common","icon":"","title":"Registration form","description":"Sign up for activities","keywords":["jensen","wa","virtualgames"],"attributes":{"backgroundColor":{"type":"string","default":"secondary"},"textColor":{"type":"string","default":"black"},"nfRegistration":{"type":"string","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-set-user-data/block.json":
/*!*************************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-set-user-data/block.json ***!
  \*************************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/set-user-data","category":"common","icon":"","title":"Finish Registration","description":"Finish user registration after email confirmation","keywords":["jensen","wa","virtualgames"],"attributes":{"backgroundColor":{"type":"string","default":"secondary"},"textColor":{"type":"string","default":"black"},"nfRegistration":{"type":"string","default":false}},"supports":{"align":["center","full"]}}');

/***/ }),

/***/ "./virtualgames-gutenberg/src/blocks/user-sign-in/block.json":
/*!*******************************************************************!*\
  !*** ./virtualgames-gutenberg/src/blocks/user-sign-in/block.json ***!
  \*******************************************************************/
/***/ (function(module) {

module.exports = JSON.parse('{"apiVersion":2,"name":"jg/signin","category":"common","icon":"","title":"Sign in form","description":"","keywords":["jensen","wa","virtualgames"],"attributes":{"backgroundColor":{"type":"string","default":"white"},"textColor":{"type":"string","default":"black"},"bg":{"type":"object","default":false},"primaryLogo":{"type":"object","default":false},"secondaryLogo":{"type":"object","default":false}},"supports":{"align":["center","full"]}}');

/***/ })

},
/******/ function(__webpack_require__) { // webpackRuntimeModules
/******/ var __webpack_exec__ = function(moduleId) { return __webpack_require__(__webpack_require__.s = moduleId); }
/******/ __webpack_require__.O(0, ["assets/styles/editor","assets/styles/app","/assets/scripts/vendor"], function() { return __webpack_exec__("./virtualgames-gutenberg/src/blocks/blocks.jsx"), __webpack_exec__("./virtualgames-gutenberg/src/styles/app.scss"), __webpack_exec__("./virtualgames-gutenberg/src/blocks/editor.scss"); });
/******/ var __webpack_exports__ = __webpack_require__.O();
/******/ }
]);