<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockProfile::class ) ) {
	class BlockProfile {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'profile' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			if ( 'jg/profile' === $block['blockName'] ) {
				$attributes = $block['attrs'];

				$user_id = BlockHelpers::get_user_id();
				$user    = new \WP_User( $user_id );

				$profile = BlockHelpers::page_link( '<h1 class="mx-auto text-center mb-4">Login Required</h1>' );

				if ( $profile ) {
					return $profile;
				}

				if ( ! in_array( 'author', (array) $user->roles ) ) {
					return BlockHelpers::cover_block( [], '<h1 class="mx-auto text-center mb-4">Error</h1><h3 class="mx-auto text-center">Please make sure that you have <u>clicked the link in your email</u> before going to your profile.</h3><h3 class="mx-auto text-center">If you have and you are seeing this message then please contact us for support.</h3>' );
				}

				$family = get_user_meta( $user_id, 'family', true );

				$profile_form  = BlockHelpers::render_form( $attributes['nfProfile'] );
				$password_form = BlockHelpers::render_form( $attributes['nfPassword'] );

				$games_ended = BlockHelpers::games_ended();
				if ( $games_ended ) {
					$profile_form = $games_ended;
				}

				$links = [[
					'slug'    => 'profile',
					'label'   => 'Update Profile',
					'content' => $profile_form,
				], 'password' => [
					'slug'    => 'password',
					'label'   => 'Update Password',
					'content' => $password_form,
				], [
					'slug'    => 'family',
					'label'   => 'Family Members',
					'content' => ( new ProfileFamily() )->render( $user_id, $attributes['nfFamily'] ),
				], [
					'slug'    => 'activity',
					'label'   => 'Activities',
					'content' => ( new GetActivities() )->render( $user_id ),
				], [
					'slug'    => 'guestbook',
					'label'   => 'Guestbook',
					'content' => ( new PostGuestbook() )->render_profile( $user_id, $attributes['nfComment'] ),
				]];

				if ( ! $password_form ) {
					unset( $links['password'] );
				}

				$classes = BlockHelpers::gutenberg_classes( ['profile'], $attributes );

				$block_content = BlockHelpers::cover_block( $classes, $this->tabs( $links ) );
			}
			return $block_content;
		}

		function tabs( $links ) {
			$tabs     = [];
			$contents = [];

			$modify_type = $_GET['type'] ? $_GET['type'] : 'activity';
			$modify_id   = isset( $_GET['id'] ) ? $_GET['id'] : false;
			if ( 'family' === $modify_type && 0 === $modify_id ) {
				$modify_type = 'profile';
			}

			foreach ( $links as $link ) {
				$slug = $link['slug'];

				$nav_classes     = [$slug, 'nav-link'];
				$content_classes = [$slug, 'tab-pane', 'fade'];

				if ( $modify_type === $slug ) {
					$nav_classes[]     = 'active';
					$content_classes[] = 'active';
					$content_classes[] = 'show';
				}

				$tabs[] = '<a class="' . implode( ' ', $nav_classes ) . '" id="v-pills-' . $slug . '-tab" data-toggle="pill" href="#v-pills-' . $slug . '" role="tab" aria-controls="v-pills-' . $slug . '" aria-selected="true">' . $link['label'] . '</a>';

				$contents[] = '<div id="v-pills-' . $slug . '" class="' . implode( ' ', $content_classes ) . '" role="tabpanel" aria-labelledby="v-pills-' . $slug . '-tab">' . $link['content'] . '</div>';
			}

			$tabs     = '<div id="v-pills-tab" class="nav flex-column nav-pills" role="tablist" aria-orientation="vertical">' . implode( $tabs ) . '</div>';
			$contents = '<div class="tab-content" id="v-pills-userTabs">' . implode( $contents ) . '</div>';

			return '<div class="d-flex align-items-start">' . $tabs . $contents . '</div>';
		}
	}

	new BlockProfile();
}