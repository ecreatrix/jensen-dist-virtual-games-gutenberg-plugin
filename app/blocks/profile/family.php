<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( ProfileFamily::class ) ) {
	class ProfileFamily {
		function render( $user_id, $form_id ) {
			$label = BlockHelpers::get_action_label( 'family', 'family member' );

			$form          = BlockHelpers::render_form( $form_id );
			$should_delete = BlockHelpers::buttons_delete( 'family', $user_id );
			if ( $should_delete ) {
				return $should_delete;
			}

			if ( $games_started ) {
				$label = '';
				$form  = '';
			}

			$games_ended = BlockHelpers::games_ended();
			if ( $games_ended ) {
				$label = '';
				$form  = $games_ended;
			}

			return $this->table( $user_id ) . $label . $form;
		}

		function table( $user_id ) {
			$family  = get_user_meta( $user_id, 'family', true );
			$members = [];

			foreach ( $family as $key => $member ) {
				if ( '' === $key ) {
					continue;
				}

				$member_name  = '<td>' . trim( $member['firstname'] . ' ' . $member['lastname'] ) . '</td>';
				$age_category = '<td>' . $member['age_category'] . '</td>';
				//$points = '<td>' . $member['points'] . '</td>';
				$shirt_size = '<td>' . $member['shirt_size'] . '</td>';

				$sports = [];
				foreach ( $member['completed_sports'] as $sportID ) {
					$sports[] = BlockHelpers::get_sport_info( $sportID );
				}
				$sports = '<td>' . implode( '<br>', $sports ) . '</td>';

				$buttons = '<td>' . BlockHelpers::buttons_change( 'family', $key ) . '</td>';
				if ( 0 === $key || 'profile' === $key ) {
					$profile_page = get_permalink( get_theme_mod( 'jg_profile' ) ) . '?type=profile';

					$profile_page = \jg\Theme\Helpers::theme_page( $post_id, 'profile' );
					$permalink    = $profile_page['permalink'] . '?type=profile';

					$buttons = '<td><span class="links"><a class="btn" href="' . $permalink . '"><i class="fas fa-edit"> Edit</i></a></span></td>';
				}

				$members[] = '<tr class="family id-' . $key . '">' . $member_name . $points . $age_category . $shirt_size . $sports . $buttons . '</tr>';
			}

			return '<div class="table-responsive"><table class="table table-striped table-hover"><thead><tr><th>Name</th><th>Age Category</th><th>Shirt Size</th><th>Completed Activities</th><th></th></tr></thead><tbody>'
			. implode( $members )
				. '</tbody></table></div>';
		}
	}

	new BlockProfile();
}