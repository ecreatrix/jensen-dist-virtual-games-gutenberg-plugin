<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( GetActivities::class ) ) {
	class GetActivities {
		static function get_media( $post_id, $link ) {
			if ( empty( $link ) ) {
				return [
					'td'    => '<td></td>',
					'modal' => '',
				];
			}

			$href = ( new \SimpleXMLElement( $link ) )['href'];

			$media_type = wp_check_filetype( $href );

			$slug = 'media-' . $post_id;

			if ( str_contains( $media_type['type'], 'video' ) ) {
				$video = '<video width="320" height="240" controls><source src="' . $href . '" type="' . $media_type['type'] . '">Your browser does not support the video tag.</video>';

				$modal = BlockHelpers::make_modal( $slug, '<i class="fas fa-play-circle"></i>', $video, 'video' );
			} else {
				$image = '<img class="media" src="' . $href . '" /><i class="fas fa-search-plus"></i>';

				$modal = BlockHelpers::make_modal( $slug, $image, $image, 'image' );
			}

			return [
				'td'    => '<td>' . $modal['button'] . '</td>',
				'modal' => $modal['content'],
			];
		}

		function render( $user_id ) {
			$label = BlockHelpers::get_action_label( 'activity' );

			$games_started = BlockHelpers::games_started();
			if ( $games_started ) {
				$label = '';
			}

			$games_ended = BlockHelpers::games_ended();
			if ( $games_ended ) {
				$label = '';
			}

			//update_user_meta( $user_id, 'points_total', 0 );

			$should_delete = BlockHelpers::buttons_delete( 'activity', $user_id );
			if ( $should_delete ) {
				return $should_delete;
			}

			return $this->table( $user_id );
		}

		static function single( $post, $user_id ) {
			$post_id   = $post->ID;
			$post_meta = get_post_meta( $post_id );

			$activity_id = $post_meta['activity'][0];
			$sport_meta  = get_post_meta( $activity_id );

			$members = get_user_meta( $user_id, 'family', true );

			$member_key = $post_meta['member'][0];

			$member = $members[$member_key];

			$member_name  = '<td>' . trim( $member['firstname'] . ' ' . $member['lastname'] ) . '</td>';
			$age_category = '<td>' . $member['age_category'] . '</td>';

			$completed = '<td>' . $post_meta['completed'][0] . '</td>';

			$points_earned = '<td>' . $post_meta['points_earned'][0] . '</td>';

			$activity = '<td>' . BlockHelpers::get_sport_info( $activity_id ) . '</td>';

			$media = self::get_media( $post_id, $post_meta['media'][0] );

			$date    = '<td>' . get_the_date( 'n-j-Y', $post ) . '</td>';
			$buttons = '<td>' . BlockHelpers::buttons_change( 'activity', $post_id ) . '</td>';

			return [
				'tr'    => '<tr class="activity id-' . $post_id . ' ' . $term->name . '">' . $member_name . $age_category . $points_earned . $activity . $completed . $media['td'] . $date . $buttons . '</tr>',
				'modal' => $media['modal'],
			];
		}

		function table( $user_id ) {
			$user_meta = get_user_meta( $user_id );

			$args = [
				'author'         => $user_id,
				'orderby'        => 'post_date',
				'post_type'      => 'user-sport',
				'order'          => 'ASC',
				'posts_per_page' => -1, // no limit
			];

			$current_user_posts = get_posts( $args );

			$activities   = [];
			$total_points = $user_meta['points_total'][0];
			foreach ( $current_user_posts as $post ) {
				$row = self::single( $post, $user_id );

				$activities['tr'][]     = $row['tr'];
				$activities['modals'][] = $row['modal'];
			}

			//$avatar = $user_meta['family_avatar'][0];
			//if ( $avatar ) {
			//$avatar = '<img class="avatar" src="' . $link['href'] . '" />';
			//}

			$members       = get_user_meta( $user_id, 'family', true );
			$member_points = ['<h4 class="single">Family: ' . $total_points . ' points</h4>'];
			foreach ( $members as $member ) {
				$member_points[] = '<h4 class="single">' . $member['firstname'] . ' ' . $member['lastname'] . ': ' . $member['points'] . ' points</h4>';
			}

			$intro = '<div class="intro"><h3 class="team">Team: ' . $user_meta['nickname'][0] . '</h3><div class="points mt-4 mb-5">' . implode( $member_points ) . '</div></div>';

			$games_started = BlockHelpers::games_started();
			if ( $games_started ) {
				return $intro . $games_started;
			}

			if ( empty( $activities['tr'] ) ) {
				$activities = [BlockHelpers::empty_table( 'single', 8 )];
			}

			return $intro . '<h4 class="mt-5 mb-3">All Activities</h4><div class="table-responsive"><table class="table table-striped table-hover"><thead><tr>
						<th>Family Member</th>
						<th>Age Group</th>
						<th>Points</th>
						<th>Activity</th>
						<th>Completed?</th>
						<th>Media</th>
						<th>Date Completed</th>
						<th></th>
					</tr></thead><tbody>
					' . implode( $activities['tr'] ) . '
				</tbody>
			</table></div>' . implode( $activities['modals'] );
		}
	}
}