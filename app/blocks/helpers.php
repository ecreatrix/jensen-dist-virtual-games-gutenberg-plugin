<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockHelpers::class ) ) {
	class BlockHelpers {
		public function __construct() {}
		static public function block_json( $name, $file = 'block.json' ) {
			return \jg\Plugin\Gutenberg\PATH . 'assets/scripts/blocks/' . $name . '/' . $file;
		}

		// Edit/delete buttons
		static function buttons_change( $type, $id ) {
			if ( 'main' === $id ) {
				return '';
			}

			$labels = [
				'edit'   => '<i class="fas fa-edit"> Edit</i>',
				'delete' => '<i class="fas fa-times"> Remove</i>',
			];
			$buttons = [];

			foreach ( $labels as $key => $label ) {
				if ( 'family' === $type && 'delete' === $key && 'profile' === $id ) {
					continue;
				} else if ( 'activity' === $type && 'edit' === $key ) {
					continue;
				}

				$buttons[] = '<a class="btn ' . $key . '" href="?type=' . $type . '&action=' . $key . '&id=' . $id . '">' . $label . '</a>';
			}

			return '<span class="links">' . implode( $buttons ) . '</span>';
		}

		// Show delete buttons
		static function buttons_delete( $type, $user_id ) {
			$delete_type = $_GET['type'] ? $_GET['type'] : false;
			if ( $type !== $delete_type ) {
				return;
			}

			$should_delete = self::should_delete( $user_id );

			$delete_id = isset( $_GET['id'] ) ? $_GET['id'] : false;
			$content   = '';

			if ( 'family' === $type ) {
				$family = get_user_meta( $user_id, 'family', true );
				$member = $family[$should_delete];
				$label  = $member['firstname'] . ' ' . $member['lastname'];

				$content = 'If you proceed, your account will be deducted ' . $member['points'] . ' points.';
			} else if ( 'activity' === $type ) {
				$post = get_post( $delete_id );

				$username = get_user_meta( $user_id, 'username', true );

				$label = substr( $post->post_title, strlen( $username ) + 3 );

				$content = 'If you proceed, your account will be deducted ' . get_post_meta( $delete_id, 'points_earned', true ) . ' points.';
			} else if ( 'guestbook' === $type ) {
				$content = 'If you proceed, your comment will be deleted.';
			}

			if ( $should_delete ) {
				global $post;
				$post_id = $post->ID;

				$profile_page = \jg\Theme\Helpers::theme_page( $post_id, 'profile' );
				$permalink    = $profile_page['permalink'] . '?type=' . $type;

				if ( ! $profile_page['current_page'] ) {
					$permalink = get_permalink( $post_id ) . '?type=' . $type;
				}

				$irreversible = '<h5 class="mb-3 text-secondary"><u>This change is permanent</u></h5>';
				$remove       = '<h5 class="mb-3">Would you like to remove ' . $label . ' from your account?</h5>';

				$content = $remove . $irreversible . '<p>' . $content . '</p><a class="btn btn-tertiary" href="' . $permalink . '&action=delete_confirm&id=' . $should_delete . '">Remove ' . $label . '</a>
				<a class="ml-4 btn btn-secondary" href="' . $permalink . '">Keep ' . $label . '</a>';

				return $content;
			}

			return false;
		}

		static function color_object() {
			return [
				'type'       => 'object',
				'properties' => [
					'color' => [
						'type'    => 'string',
						'default' => '#ffffff',
						'format'  => 'hex-color',
					],
					'name'  => [
						'type'    => 'string',
						'default' => '',
					],
				],
			];
		}

		static function cover_block( $classes, $content, $attributes = '' ) {
			$classes[] = 'wp-block-cover jg';

			$style = '';
			if ( ! empty( $attributes ) ) {
				$style = ' style="' . implode( '; ', $attributes ) . '"';
			}

			return '<div class="' . implode( ' ', $classes ) . '"' . $style . '><div class="wp-block-cover__inner-container">' . $content . '</div></div>';
		}

		static function empty_table( $type, $colspan = 1 ) {
			return '<td colspan="' . $colspan . '">No ' . $type . ' entries yet</td>';
		}

		static function games_ended() {
			$end_date = strtotime( get_theme_mod( 'jg_game_end' ) );

			if ( $end_date < strtotime( 'now' ) ) {
				// Games have ended
				return '<h4 class="mt-4">The Games are closed. Thank you for the participation!</h4>';
			}

			return false;
		}

		static function games_started() {
			$start_date = strtotime( get_theme_mod( 'jg_game_start' ) );

			if ( $start_date > strtotime( 'now' ) ) {
				// Games have not started
				return '<h4 class="mt-4">The Virtual Games have not started yet. Please check back after <u>' . date( 'F j, Y', $start_date ) . '</u></h4>';
			}

			return false;
		}

		static function generic_error_message() {
			return self::cover_block( [], '<h1 class="mx-auto text-center mb-4">Error</h1><h3 class="mx-auto text-center">Please contact us for support.</h3>' );
		}

		static function get_action_label( $tab_slug, $tab_label = false ) {
			$label = 'Add';

			if ( ! $tab_label ) {
				$tab_label = $tab_slug;
			}

			if ( $_GET['action'] && $_GET['type'] && $tab_slug === $_GET['type'] && 'delete_confirm' !== $_GET['action'] ) {
				$label = 'delete' === $_GET['action'] ? 'Remove' : 'Modify';
			}

			return '<hr><h4 class="mt-5 mb-3">' . $label . ' ' . $tab_label . '</h4>';
		}

		static function get_sport_info( $sportID ) {
			$sport      = get_post( $sportID );
			$sport_meta = get_post_meta( $sportID );

			$terms = get_the_terms( $sportID, 'sport_tax' );

			return $terms[0]->name . ' - ' . $sport->post_title;
		}

		static function get_sports_terms( $attributes ) {
			$terms = get_terms( [
				'taxonomy'   => 'sport_tax',
				'per_page'   => 1,
				'hide_empty' => false,
			] );

			if ( array_key_exists( 'category', $attributes ) && $attributes['category'] && $attributes['category'] && 'false' !== $attributes['category'] ) {
				$terms = get_term_by( 'slug', $attributes['category'], 'sport_tax' );
			}

			return $terms;
		}

		static function get_user_id() {
			global $current_user;
			get_currentuserinfo();
			$user_id = $current_user->ID;

			return $user_id;
		}

		static function gutenberg_classes( $classes, $attributes ) {
			if ( array_key_exists( 'align', $attributes ) ) {
				$classes[] = "align{$attributes['align']}";
			} else {
				$classes[] = 'alignfull';
			}

			if ( array_key_exists( 'backgroundColor', $attributes ) ) {
				$classes[] = "has-{$attributes['backgroundColor']}-background-color";
			}

			if ( array_key_exists( 'textColor', $attributes ) ) {
				$classes[] = "has-{$attributes['textColor']}-text-color";
			}

			if ( array_key_exists( 'className', $attributes ) ) {
				$classes[] = $attributes['className'];
			}

			return $classes;
		}

		static public function make_modal( $slug, $button_content, $modal_content, $button_class = '' ) {
			$classes = ['btn btn-link btn-modal', $button_class];
			$button  = '<a class="' . implode( ' ', $classes ) . '" data-toggle="modal" data-target="#' . $slug . '">' . $button_content . '</a>';

			$modal = '<div class="modal fade" id="' . $slug . '" tabindex="-1" role="dialog" aria-labelledby="' . $slug . '" aria-hidden="true"><div class="modal-dialog modal-dialog-centered modal-xl" role="document"><div class="modal-content"><div class="modal-header"><h5 class="modal-title" id="' . $slug . '-title">UpformLoaded Media</h5><button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button></div><div class="modal-body">' . $modal_content . '</div>  <div class="modal-footer"><button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button></div></div></div></div>';

			return [
				'button'  => $button,
				'content' => $modal,
			];
		}

		static function page_link( $content ) {
			global $post;
			$post_id = $post->ID;

			$user = get_user_by( 'ID', self::get_user_id() );

			if ( ! method_exists( '\jg\Theme\Helpers', 'theme_page' ) ) {
				return false;
			}

			$profile_page          = \jg\Theme\Helpers::theme_page( $post_id, 'profile' );
			$signin_page           = \jg\Theme\Helpers::theme_page( $post_id, 'signin' );
			$registration_page     = \jg\Theme\Helpers::theme_page( $post_id, 'registration_start' );
			$registration_complete = \jg\Theme\Helpers::theme_page( $post_id, 'registration_complete' );

			if ( $signin_page['current_page'] && is_user_logged_in() ) {
				// On signing page page and logged in, show profile link

				return self::cover_block( ['redirect-required'], $content . '<h3 class="mx-auto text-center"><a href="' . $profile_page['permalink'] . '" style="color: inherit; text-decoration: underline">View your profile here</a></h3>' );
			} else if ( $profile_page['current_page'] && ! is_user_logged_in() ) {
				// On profile page and not logged in, show registration link

				return self::cover_block( ['redirect-required'], $content . '<h3 class="mx-auto text-center"><a href="' . $signin_page['permalink'] . '" style="color: inherit; text-decoration: underline">Sign in here</a></h3>' );
			} else if ( $registration_page['current_page'] && is_user_logged_in() ) {
				// On registration page and logged in, show profile link

				return self::cover_block( ['redirect-required'], $content . '<h3 class="mx-auto text-center"><a href="' . $profile_page['permalink'] . '" style="color: inherit; text-decoration: underline">View your profile here</a></h3>' );
			} else if ( $registration_complete['current_page'] && is_user_logged_in() && ! in_array( 'subscriber', (array) $user->roles ) ) {
				// On finish registration

				return self::cover_block( ['redirect-required'], $content . '<h3 class="mx-auto text-center"><a href="' . $profile_page['permalink'] . '" style="color: inherit; text-decoration: underline">View your profile here</a></h3>' );
			}

			return false;
		}

		static function render_form( $form_id ) {
			if ( ! $form_id ) {
				return ''; //self::generic_error_message();
			}

			ob_start();
			Ninja_Forms()->display( $form_id );

			return ob_get_clean();

			//return do_shortcode( '[ninja_form id=' . $form_id . ']' );
		}

		// Delete post and user info if on delete_confirm page
		static public function should_delete( $user_id ) {
			$delete_type   = isset( $_GET['type'] ) ? $_GET['type'] : false;
			$delete_id     = isset( $_GET['id'] ) ? $_GET['id'] : false;
			$delete_action = isset( $_GET['action'] ) ? $_GET['action'] : false;

			if ( $delete_type && 'delete' === $delete_action && false !== $delete_id ) {
				return $delete_id;
			} else if ( $delete_type && 'delete_confirm' === $delete_action && false !== $delete_id ) {
				global $post;
				$post_id      = $post->ID;
				$profile_page = \jg\Theme\Helpers::theme_page( $post_id, 'profile' );
				$permalink    = $profile_page['permalink'] . '?type=' . $delete_type;

				$family        = get_user_meta( $user_id, 'family', true );
				$family_points = get_user_meta( $user_id, 'points_total', true );

				if ( 'family' === $delete_type ) {
					$member = $family[$delete_id];

					// Remove member's totals from family
					$family_points -= $member['points'];
					update_user_meta( $user_id, 'points_total', $family_points );

					// remove member from family
					unset( $family[$delete_id] );
					update_user_meta( $user_id, 'family', $family );

					// remove all sport posts from this user with member
					$args = [
						'numberposts' => -1, // The number of posts to retrieve, otherwise 5
						'post_type'   => 'user-sport',
						'post_status' => 'publish',
						'meta_query'  => [
							[
								'key'   => 'member',
								'value' => $delete_id,
							],
						],
					];
					$posts = get_posts( $args );
					foreach ( $posts as $post ) {
						wp_delete_post( $post->ID );
					}
				} else if ( 'activity' === $delete_type ) {
					$sport_id  = get_post_meta( $delete_id, 'activity', true );
					$member_id = get_post_meta( $delete_id, 'member', true );

					$member = $family[$member_id];

					$search_user_post_id = array_search( $sport_id, $member['completed_sports'] );
					if ( false !== $search_user_post_id ) {
						$points_earned = get_post_meta( $search_user_post_id, 'points_earned', true );
						// Remove activity from member
						unset( $family[$member_id]['completed_sports'][$search_user_post_id] );

						// Remove activity's points from user's points total
						$user_points = get_user_meta( $user_id, 'points_total', true ) - $points_earned;
						$family[$member_id]['points'] -= $points_earned;

						update_user_meta( $user_id, 'points_total', $user_points );
						update_user_meta( $user_id, 'family', $family );
					}

					// Remove activity post
					wp_delete_post( $delete_id );
				} else if ( 'guestbook' === $delete_type ) {
					// Remove guestbook post
					if ( ! $profile_page['current_page'] ) {
						$permalink = get_permalink( $post_id );
					}
					wp_delete_post( $delete_id );
				}

				echo '<script>location.href = \'' . $permalink . '\'</script>';
			}

			return false;
		}
	}

	new BlockHelpers();
}