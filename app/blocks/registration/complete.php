<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockFinishRetistration::class ) ) {
	class BlockFinishRetistration {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'set-user-data' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			// Remove the block/timed-block from the rendered content.
			if ( 'jg/set-user-data' === $block['blockName'] ) {
				$attributes = $block['attrs'];

				$user_submission_key = $_GET['id'];
				if ( false && 'reset-points' == $user_submission_key ) {
					$args = [
						'role' => 'author',
					];

					$users = get_users( $args );

					foreach ( $users as $user ) {
						$user_id      = $user->ID;
						$family       = get_user_meta( $user_id, 'family', true );
						$points       = get_user_meta( $user_id, 'points', true );
						$points_total = get_user_meta( $user_id, 'points_total', true );
						$user_data    = get_userdata( $user_id );

						//update_user_meta( $user_id, 'family_2021', $family );
						//update_user_meta( $user_id, 'points_2021', $points );
						//update_user_meta( $user_id, 'points_total_2021', $points_total );

						//update_user_meta( $user_id, 'family', [] );
						//update_user_meta( $user_id, 'points', 0 );
						//update_user_meta( $user_id, 'points_total', [] );

						//var_dump( $family );
						echo '<br>';
						var_dump( get_user_meta( $user_id ) );
						echo '<br>';
						echo '<br>';
					}
					return false;
				}
				$error = BlockHelpers::cover_block( [], '<h1 class="mx-auto text-center mb-4">Error</h1><h3 class="mx-auto text-center">Please contact us for support.</h3>' );

				if ( ! $user_submission_key || ! isset( $user_submission_key ) || ! $attributes['nfRegistration'] ) {
					// Submission ID/Form ID missing
					return $error;
				}

				$loggedIn = BlockHelpers::page_link( '<h1 class="mx-auto text-center mb-4">Already logged-in</h1>' );
				if ( 'reset' !== $user_submission_key && $loggedIn ) {
					return $loggedIn;
				}

				// On registration page but no id set or Ninja Forms not active
				if ( ! class_exists( 'Ninja_Forms' ) ) {
					return $error;
				}

				$form_id = $attributes['nfRegistration'];
				$finish  = \jg\Plugin\User\NinjaHelpers::finish_registration( $form_id, $user_submission_key );

				if ( ! $finish ) {
					return $error;
				}

				$block_content = BlockHelpers::cover_block( [], '<h1 class="mx-auto text-center mb-4">Thank you for registering</h1><h3 class="mx-auto text-center"><a href="' . get_permalink( get_theme_mod( 'jg_signin' ) ) . '" style="color: inherit; text-decoration: underline">Log in here</a></h3>' );
			}

			return $block_content;
		}
	}

	new BlockFinishRetistration();
}