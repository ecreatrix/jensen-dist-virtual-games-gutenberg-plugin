<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockSignin::class ) ) {
	class BlockSignin {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'signin' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			if ( 'jg/signin' === $block['blockName'] ) {
				$attributes = $block['attrs'];

				$logged_in = BlockHelpers::page_link( '<h1 class="mx-auto text-center mb-4">Already logged-in</h1>' );
				if ( $logged_in ) {
					return $logged_in;
				}

				$classes = BlockHelpers::gutenberg_classes( ['sign-in', 'alignfull'], $attributes );

				$logos = '<div class="logos">'
				. \jg\Theme\Helpers::mod_image( 'jg_signin_page_secondary_logo', false, ['class' => 'secondary'] )
				. \jg\Theme\Helpers::mod_image( 'jg_signin_page_primary_logo', false, ['class' => 'primary'] )
					. '</div>';

				$form = wp_login_form( [
					'redirect' => get_permalink( get_theme_mod( 'jg_profile' ) ),
					'echo'     => false,
				] );

				$lost_password = '<a class="lost-password" href="' . esc_url( wp_lostpassword_url() ) . '" alt="Lost Password">Forgot Password?</a>';

				$form = str_replace( 'name="log"', 'name="log" placeholder="Username"', $form );
				$form = str_replace( 'name="pwd"', 'name="pwd" placeholder="Password"', $form );

				$form = str_replace( '<p class="login-remember">', '<p class="login-remember">' . $lost_password, $form );

				$form = '<div class="form"><h3 class="title">Sign In</h3>' . $form . '</div>';

				$block_content = BlockHelpers::cover_block( $classes, $logos . $form, [\jg\Theme\Helpers::mod_image( 'jg_signin_page_bg', true )] );
			}

			return $block_content;
		}
	}

	new BlockSignin();
}