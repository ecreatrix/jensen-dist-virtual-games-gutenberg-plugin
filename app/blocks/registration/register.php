<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockRegistration::class ) ) {
	class BlockRegistration {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'register' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			if ( 'jg/register' === $block['blockName'] ) {
				$attributes = $block['attrs'];

				$registered = BlockHelpers::page_link( '<h1 class="mx-auto text-center mb-4">Already logged-in</h1>' );
				if ( $registered ) {
					return $registered;
				}

				$form = BlockHelpers::render_form( $attributes['nfRegistration'] );

				$games_ended = BlockHelpers::games_ended();
				if ( $games_ended ) {
					$form = $games_ended;
				}

				$classes = BlockHelpers::gutenberg_classes( ['register'], $attributes );

				$block_content = BlockHelpers::cover_block( $classes, $form );
			}

			return $block_content;
		}
	}

	new BlockRegistration();
}