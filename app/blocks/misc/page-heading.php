<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlockPageHeading::class ) ) {
	class BlockPageHeading {
		public function __construct() {
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'page-heading' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			if ( 'jg/page-heading' === $block['blockName'] ) {
				$attributes = $block['attrs'];

				if ( ! array_key_exists( 'backgroundColor', $attributes ) ) {
					$attributes['backgroundColor'] = 'secondary';
				}
				if ( ! array_key_exists( 'textColor', $attributes ) ) {
					$attributes['textColor'] = 'white';
				}
				if ( ! array_key_exists( 'align', $attributes ) ) {
					$attributes['align'] = 'full';
				}

				$content = '<h1 class="entry-title">' . get_the_title() . '</h1>';

				$classes = BlockHelpers::gutenberg_classes( ['profile'], $attributes );

				$block_content = BlockHelpers::cover_block( $classes, $content );
			}

			return $block_content;
		}
	}

	new BlockPageHeading();
}