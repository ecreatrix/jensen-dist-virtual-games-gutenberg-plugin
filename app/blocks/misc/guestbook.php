<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( PostGuestbook::class ) ) {
	class PostGuestbook {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'register_routes'] );

			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function content( $attributes ) {
			$classes = BlockHelpers::gutenberg_classes( ['guestbook'], $attributes );
			$args    = [
				'post_type'   => 'guestbook',
				'numberposts' => -1,
				'order'       => 'DESC',
				'orderby'     => 'date',
			];

			$posts = get_posts( $args );

			$submitted_entries = [];
			if ( ! empty( $posts ) ) {
				foreach ( $posts as $post ) {
					$submitted_entries[] = self::single_comment( $post );
				}
			}

			$submitted_entries = implode( $submitted_entries );

			if ( ! is_user_logged_in() ) {
				return BlockHelpers::cover_block( ['login-required'], $content . '<h3 class="mx-auto text-center">Please note that you must be registered as a participant to leave a comment on the Well Nation Virtual Games guestbook.</h3>' ) . BlockHelpers::cover_block( $classes, $submitted_entries );
			}

			$user_id = BlockHelpers::get_user_id();
			$content = $this->render_profile( $user_id, $attributes['nfComment'] );

			return BlockHelpers::cover_block( $classes, $submitted_entries . $content );
		}

		function limit_entries( $count, $form_id ) {
			$games_ended = BlockHelpers::games_ended();
			if ( $games_ended ) {
				return $games_ended;
			}

			if ( $count < 3 ) {
				return BlockHelpers::render_form( $form_id );
			} else {
				return 'You have already submitted the maximum number of entries. Please delete or modify an existing post if you\'d like to make an entry.';
			}
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'guestbook' );

			register_block_type_from_metadata( $block_json_file );
		}

		function register_routes() {
			\register_rest_route( 'jensen/v1', '/posts/gutenberg/', [
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => [$this, 'content'],
				'permission_callback' => function () {
					return true;
				},
			] );
		}

		// Only render if a single term is returned which means that a category was set
		function render( $block_content, $block ) {
			// Remove the block/timed-block from the rendered content.
			if ( 'jg/guestbook' === $block['blockName'] ) {
				$attributes = $block['attrs'];

				$block_content = $this->content( $attributes );
			}

			return $block_content;
		}

		function render_profile( $user_id, $form_id ) {
			$should_delete = BlockHelpers::buttons_delete( 'guestbook', $user_id );
			if ( $should_delete ) {
				return $should_delete;
			}

			$intro = '<h4 class="mt-5 mb-3">Your Guestbook Entries</h4>';

			$table = $this->table( $user_id );

			$form = $this->limit_entries( $table['count'], $form_id );

			$label = BlockHelpers::get_action_label( 'guestbook', 'guestbook entry' );

			return $intro . $table['content'] . $label . $form;
		}

		static function single_comment( $post ) {
			$post_id = $post->ID;
			$classes = ['entry', 'id-' . $post_id];

			$title   = '<h5 class="author">' . get_the_title( $post_id ) . '</h5>';
			$content = '<div class="content">' . $post->post_content . '</div>';

			return '<blockquote class="' . implode( ' ', $classes ) . '">' . $content . $title . '</blockquote>';
		}

		static function single_row( $post, $user_id ) {
			$post_id   = $post->ID;
			$post_meta = get_post_meta( $post_id );

			$title   = '<td>' . get_the_title( $post_id ) . '</td>';
			$comment = '<td>' . apply_filters( 'the_content', get_post_field( 'post_content', $post_id ) ) . '</td>';
			$date    = '<td>' . get_the_date( '', $post ) . '</td>';
			$buttons = '<td>' . BlockHelpers::buttons_change( 'guestbook', $post_id ) . '</td>';

			return '<tr class="guestbook id-' . $post_id . '">' . $title . $comment . $date . $buttons . '</tr>';
		}

		function table( $user_id ) {
			$user_meta = get_user_meta( $user_id );

			$args = [
				'author'         => $user_id,
				'orderby'        => 'post_date',
				'post_type'      => 'guestbook',
				'order'          => 'ASC',
				'posts_per_page' => -1, // no limit
			];

			$current_user_posts = get_posts( $args );

			$entries = [];
			foreach ( $current_user_posts as $post ) {
				$entries[] = self::single_row( $post, $user_id );
			}

			if ( empty( $entries ) ) {
				$entries = [BlockHelpers::empty_table( 'guestbook', 4 )];
			}

			return [
				'count'   => count( $entries ),
				'content' => '<div class="table-responsive"><table class="table table-striped table-hover"><thead><tr>
					<th>Title</th>
					<th>Comment</th>
					<th>Date</th>
					<th></th>
				</tr></thead><tbody>
				' . implode( $entries ) . '
			</tbody>
		</table></div>',
			];
		}
	}

	new PostGuestbook();
}