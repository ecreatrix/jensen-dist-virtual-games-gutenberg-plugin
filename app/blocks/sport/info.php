<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlocksSportInfo::class ) ) {
	class BlocksSportInfo {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'rest_register'], 20 );
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'activity-info' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			if ( 'jg/sport-info' === $block['blockName'] ) {
				$attributes = $block['attrs'];
				$attributes = $block['attrs'];

				$term    = BlockHelpers::get_sports_terms( $attributes );
				$content = '';

				if ( empty( $term ) ) {
					$content = $term;
				} else if ( count( $term ) == 1 ) {
					$term_id = $term->term_id;

					$classes = ['sport-info', 'id-' . $term_id];
					$classes = BlockHelpers::gutenberg_classes( $classes, $attributes );

					$content = self::render_single_intro( $term ) . self::render_single_posts( $term, $attributes['nfPost'] );

					$content       = BlockHelpers::cover_block( $classes, $content );
					$block_content = $content;
				}

				$block_content = $content;
			}

			return $block_content;
		}

		static function render_single_intro( $term ) {
			// Equipment info
			$equipment_block = self::sport_info_block( $term, 'tax_equipment' );

			$description = $term->description;
			if ( $description ) {
				$description = '<div class="description col">' . $description . '</div>';
			}

			return '<div class="intro row">' . $description . $equipment_block . '</div>';
		}

		static function render_single_posts( $term, $form_id ) {
			$args = [
				'post_type'   => 'sport',
				'numberposts' => -1,
				'order'       => 'ASC',
				'orderby'     => 'menu_order',
				'tax_query'   => [
					[
						'taxonomy' => 'sport_tax',
						'field'    => 'term_id',
						'terms'    => $term->term_id,
					],
				],
			];

			$posts = get_posts( $args );

			$output = [];
			if ( empty( $posts ) ) {
				$output = ['No activities are available yet in the ' . $term->name . ' category.'];
			} else if ( ! is_array( $posts ) ) {
				$output = [$posts];
			} else {
				$count = 0;
				foreach ( $posts as $post ) {
					$count++;
					$post_id = $post->ID;
					$key     = $post_id;
					$classes = ['post', 'id-' . $post_id];

					$title   = '<h2 class="title">' . get_the_title( $post_id ) . '</h2>';
					$content = '<div class="content">' . apply_filters( 'the_content', $post->post_content ) . '</div>';

					$media = function_exists( 'get_field' ) ? get_field( 'jg_featured_media', $post_id ) : false;
					if ( $media ) {
						$media = '<div class="media">' . wp_video_shortcode( ['mp4' => $media] ) . '</div>';
					}

					$submit = '';

					$user_id = BlockHelpers::get_user_id();
					$user    = new \WP_User( $user_id );

					$games_started = BlockHelpers::games_started();
					$games_ended   = BlockHelpers::games_ended();
					if ( ! $games_ended && ! $games_started && is_user_logged_in() && in_array( 'author', (array) $user->roles ) ) {
						// Only show form on first block and use CSS to position
						//$form = '<div class="make-form empty"></div>';
						//if ( 1 == $count ) {
						$form = BlockHelpers::render_form( $form_id );
						//}

						$submit = '<a class="add-sport btn btn-primary" data-toggle="collapse" href="#collapse' . $post_id . '" role="button" aria-expanded="false" aria-controls="collapse' . $post_id . '">do it now</a>';
						$submit .= '<div class="collapse multi-collapse mt-4" id="collapse' . $post_id . '">' . $form . '</div>';
					}
					$materials = self::sport_info_block( $post_id, 'materials_needed', false );
					$tips      = self::sport_info_block( $post_id, 'additional_tips', false );

					$content = '<div class="row"><div class="content">' . apply_filters( 'the_content', $post->post_content ) . $materials . $tips . $submit . '</div>' . $media . '</div>';

					$points = function_exists( 'get_field' ) ? get_field( 'jg_sport_points', $post_id ) : false;
					if ( $points ) {
						$key    = $points . '_' . $post_id;
						$points = '<h4 class="points">' . $points . ' points</h4>';
					}

					$challenges     = self::sport_info_block( $post_id, 'challenges' );
					$challenge_zone = self::sport_info_block( $post_id, 'challenge_zone' );
					$highlight_reel = self::sport_info_block( $post_id, 'highlight_reel' );
					$info_block     = '<div class="info row">' . $challenges . $challenge_zone . $highlight_reel . '</div>';

					$output[$key] = '<div class="' . implode( ' ', $classes ) . '">' . $title . $points . $content . $info_block . '</div>';
				}
			}

			return '<div class="posts">' . implode( $output ) . '</div>';
		}

		static function rest_callback( $object ) {
			$term_id = $object['id'];
			$term    = get_term_by( 'id', $term_id, 'sport_tax' );
			$form_id = $object['form_id'];

			return [
				'slug'    => $term->slug,
				'content' => self::render_single_intro( $term ) . self::render_single_posts( $term, $form_id ),
			];
		}

		function rest_register() {
			register_rest_field( 'sport_tax', 'jg_sport_tax_info', [
				'get_callback' => [$this, 'rest_callback'],
				'schema'       => [
					'description' => __( 'Return single activity category\'s intro and posts', 'jg_theme' ),
					'type'        => 'object',
				],
			] );
		}

		static function sport_info_block( $item, $key, $add_classes = true ) {
			$content = '';
			if ( function_exists( 'have_rows' ) && have_rows( 'jg_sport_' . $key, $item ) ) {
				$items = [];
				while ( have_rows( 'jg_sport_' . $key, $item ) ) {
					the_row();
					$items[] = '<li>' . apply_filters( 'the_content', get_sub_field( 'jg_sport_' . $key . '_item' ) ) . '</li>';
				}

				$classes = '';
				if ( $add_classes ) {
					$classes = ' block col';
				}

				$object  = get_field_object( 'jg_sport_' . $key, $item );
				$content = '<div class="' . $key . $classes . '"><div class="inner"><div class="label">' . $object['label'] . ':</div><ul>' . implode( $items ) . '</ul></div></div>';
			}

			return $content;
		}
	}

	new BlocksSportInfo();
}