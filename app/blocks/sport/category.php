<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlocksSportCategory::class ) ) {
	class BlocksSportCategory {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'rest_register'], 20 );
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'activity-categories' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			if ( 'jg/sport-categories' === $block['blockName'] ) {
				$attributes = $block['attrs'];
				$terms      = BlockHelpers::get_sports_terms( $attributes );

				$output     = [];
				$categories = $attributes['categories'];

				foreach ( $terms as $term ) {
					if ( in_array( $term->slug, $categories ) ) {
						$output[] = self::render_single( $term );
					}
				}

				$block_content = '<div class="sport-categories"><div class="sports-row">' . implode( $output ) . '</div></div>';
			}

			return $block_content;
		}

		static function render_single( $term ) {
			$term_id = $term->term_id;
			$classes = ['category', 'id-' . $term_id];
			$tag     = 'div';
			$href    = '';

			$archive = function_exists( 'get_field' ) ? get_field( 'jg_sport_tax_archive', $term ) : false;
			if ( $archive ) {
				$tag  = 'a';
				$href = ' href="' . $archive . '"';
			}

			$icon_id   = function_exists( 'get_field' ) ? get_field( 'jg_sport_tax_icon', $term ) : false;
			$icon_path = wp_get_original_image_path( $icon_id );
			$icon_url  = wp_get_attachment_url( $icon_id );
			$icon      = '';
			if ( $icon_id && file_exists( $icon_path ) ) {
				$icon_metadata = wp_get_attachment_metadata( $icon_id );
				$icon_type     = wp_check_filetype( $icon_url );

				if ( 'svg' === $icon_type['ext'] ) {
					$icon = file_get_contents( $icon_path );
				}

				$icon = '<div class="icon">' . $icon . '</div>';
			}

			$name = '<div class="name">' . $term->name . '</div>';

			return '<' . $tag . ' class="' . implode( ' ', $classes ) . '"' . $href . '>' . $icon . $name . '</' . $tag . '>';
		}

		static function rest_callback( $object ) {
			$term_id = $object['id'];
			$term    = get_term_by( 'id', $term_id, 'sport_tax' );

			return self::render_single( $term );
		}

		function rest_register() {
			register_rest_field( 'sport_tax', 'jg_sport_tax', [
				'get_callback' => [$this, 'rest_callback'],
				'schema'       => [
					'description' => __( 'Return all sports categories', 'jg_theme' ),
					'type'        => 'object',
				],
			] );
		}
	}

	new BlocksSportCategory();
}