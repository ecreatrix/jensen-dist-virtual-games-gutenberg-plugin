<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlocksSportHeading::class ) ) {
	class BlocksSportHeading {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'rest_register'], 20 );
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'activity-heading' );

			register_block_type_from_metadata( $block_json_file );
		}

		function render( $block_content, $block ) {
			if ( 'jg/sport-heading' === $block['blockName'] ) {
				$attributes = $block['attrs'];

				if ( ! array_key_exists( 'backgroundColor', $attributes ) ) {
					$attributes['backgroundColor'] = 'secondary';
				}
				if ( ! array_key_exists( 'textColor', $attributes ) ) {
					$attributes['textColor'] = 'white';
				}
				if ( ! array_key_exists( 'align', $attributes ) ) {
					$attributes['align'] = 'full';
				}

				$term = BlockHelpers::get_sports_terms( $attributes );

				if ( empty( $term ) ) {
					$output = $term;
				} else if ( count( $term ) == 1 ) {
					$term_id = $term->term_id;

					$classes = ['sport-heading', 'id-' . $term_id];
					$classes = BlockHelpers::gutenberg_classes( $classes, $attributes );

					$output = BlockHelpers::cover_block( $classes, self::render_single( $term )['content'] );
				}

				$block_content = $output;
			}

			return $block_content;
		}

		static function render_single( $term ) {
			$image = function_exists( 'get_field' ) ? get_field( 'jg_sport_tax_heading_image', $term ) : false;
			if ( $image ) {
				$image = '<div class="image">' . wp_get_attachment_image( $image, 'full' ) . '</div>';
			}

			return [
				'slug'    => $term->slug,
				'content' => '<div class="row">' . $image . '<h1 class="title">' . $term->name . '</h1>' . '</div>',
			];
		}

		static function rest_callback( $object ) {
			$term_id = $object['id'];
			$term    = get_term_by( 'id', $term_id, 'sport_tax' );

			return self::render_single( $term );
		}

		function rest_register() {
			register_rest_field( 'sport_tax', 'jg_sport_tax_heading', [
				'get_callback' => [$this, 'rest_callback'],
				'schema'       => [
					'description' => __( 'Return single activity category heading', 'jg_theme' ),
					'type'        => 'object',
				],
			] );
		}
	}

	new BlocksSportHeading();
}