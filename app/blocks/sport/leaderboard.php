<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( BlocksSportLeaderboard::class ) ) {
	class BlocksSportLeaderboard {
		public function __construct() {
			add_action( 'rest_api_init', [$this, 'register_routes'] );
			add_action( 'init', [$this, 'register_block'], 20 );
			add_filter( 'render_block', [$this, 'render'], 10, 2 );
		}

		function content( $attributes ) {
			$ages    = $attributes['ages'];
			$content = [];
			$count   = 0;

			foreach ( $ages as $age_range ) {
				$table = $this->generate_table( $age_range, $attributes['limit'] );

				if ( $age_range && 'false' !== $age_range ) {
					$count++;

					$heading_classes[] = 'text-center';
					if ( $count >= 2 ) {
						$heading_classes[] = 'mt-5';
					}

					$content[] = '<h3 class="' . implode( ' ', $heading_classes ) . '">Ages: ' . $age_range . '</h3><div class="table-responsive"><table class="table table-striped table-hover"><thead><tr>
						<th>Family</th>
						<th>Member</th>
						<th>Points</th>
					</tr></thead><tbody>
					' . implode( $table ) . '
				</tbody>
			</table></div>';
				} else {
					$content = ['<h3 class="text-center">The leaderboard isn\'t available yet.</h3>'];
				}
			}

			$classes = BlockHelpers::gutenberg_classes( ['leaderboard'], $attributes );

			return BlockHelpers::cover_block( $classes, implode( $content ) );
		}

		function generate_table( $age_range, $limit ) {
			$games_started = BlockHelpers::games_started();
			if ( $games_started ) {
				return BlockHelpers::cover_block( ['not-started'], $games_started );
			}

			$games_ended = BlockHelpers::games_ended();
			if ( $games_ended ) {
				return BlockHelpers::cover_block( ['ended'], $games_ended );
			}

			$args = [
				'fields' => ['ID', 'user_nicename'],
			];

			$users = \get_users( $args );

			$table = [];

			if ( '20+' === $age_range ) {
				$age_range = '20-99';
			}

			$age_range = array_map( 'intval', explode( '-', $age_range ) );
			foreach ( $users as $user ) {
				$user_id   = $user->ID;
				$family    = get_user_meta( $user_id, 'family', true );
				$user_data = get_userdata( $user_id );

				$tr = [
					'nicename' => '<td>' . $user_data->user_nicename . '</td>',
				];

				foreach ( $family as $member ) {
					$points = $member['points'];
					$age    = intval( $member['age'] );

					if ( 0 != $points && $age >= $age_range[0] && $age < $age_range[1] ) {
						$table_key = $points . '-' . $user_data->username . '-' . $member['firstname'] . '-' . $member['lastname'];

						$tr['member'] = '<td>' . substr( $member['firstname'], 0, 1 ) . ' ' . substr( $member['lastname'], 0, 1 ) . '</td>';
						$tr['points'] = '<td>' . $points . ' ' . $age . '</td>';

						$table[$table_key] = '<tr>' . implode( $tr ) . '</tr>';
					}
				}
			}

			$table = array_slice( $table, 0, $limit );

			return $table;
		}

		function register_block() {
			$block_json_file = BlockHelpers::block_json( 'activity-leaderboard' );

			register_block_type_from_metadata( $block_json_file );
		}

		function register_routes() {
			\register_rest_route( 'jensen/v1', '/sports/leaderboard/', [
				'methods'             => \WP_REST_Server::EDITABLE,
				'callback'            => [$this, 'register_routes_content'],
				'args'                => [
					'attributes' => [],
				],
				'permission_callback' => function () {
					return true;
				},
			] );
		}

		function register_routes_content( \WP_REST_Request $request ) {
			$attributes = $request->get_param( 'attributes' );

			return $this->content( $attributes );
		}

		function render( $block_content, $block ) {
			if ( 'jg/sport-leaderboard' === $block['blockName'] ) {
				$attributes = $block['attrs'];

				if ( ! array_key_exists( 'backgroundColor', $attributes ) ) {
					$attributes['backgroundColor'] = false;
				}
				if ( ! array_key_exists( 'textColor', $attributes ) ) {
					$attributes['textColor'] = 'black';
				}
				if ( ! array_key_exists( 'align', $attributes ) ) {
					$attributes['align'] = 'full';
				}

				$block_content = $this->content( $attributes );
			}

			return $block_content;
		}
	}

	new BlocksSportLeaderboard();
}