<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}

if ( ! class_exists( CptSport::class ) ) {
    class CptSport {
        public function __construct() {
            add_action( 'init', [$this, 'register_post_type'] );
            add_action( 'init', [$this, 'register_tax'] );
        }

        public function register_post_type() {
            $labels = [
                'name'           => _x( 'Activities', 'Post Type General Name', 'jg_theme' ),
                'singular_name'  => _x( 'Sport', 'Post Type Singular Name', 'jg_theme' ),
                'menu_name'      => __( 'Activities', 'jg_theme' ),
                'name_admin_bar' => __( 'Sport', 'jg_theme' ),
                //'archives'       => __( 'Activities Archives', 'jg_theme' ),
                'attributes'     => __( 'Activities Attributes', 'jg_theme' )
            ];

            $args = [
                'label'               => __( 'Activity', 'jg_theme' ),
                'description'         => __( 'Virtual Game Activities', 'jg_theme' ),
                'labels'              => $labels,
                'supports'            => ['title', 'editor', 'custom-fields', 'page-attributes'],
                'taxonomies'          => ['sports_tax'],
                'hierarchical'        => false,
                'public'              => true,
                'show_ui'             => true,
                'show_in_menu'        => true,
                'menu_position'       => 5,
                'menu_icon'           => 'dashicons-megaphone',
                'show_in_admin_bar'   => true,
                'show_in_nav_menus'   => true,
                'can_export'          => true,
                'has_archive'         => false,
                'exclude_from_search' => true,
                'publicly_queryable'  => true,
                'capability_type'     => 'page',
                'show_in_rest'        => true
            ];
            register_post_type( 'sport', $args );
        }

        public function register_tax() {
            $labels = [
                'name'          => _x( 'Categories', 'Taxonomy General Name', 'jg_theme' ),
                'singular_name' => _x( 'Category', 'Taxonomy Singular Name', 'jg_theme' ),
                'all_items'     => __( 'All Categories', 'text_domain' ),
                'add_new_item'  => __( 'Add New Category', 'text_domain' ),
                'add_new'       => __( 'Add New', 'text_domain' ),
                'new_item'      => __( 'New Category', 'text_domain' ),
                'edit_item'     => __( 'Edit Category', 'text_domain' ),
                'update_item'   => __( 'Update Category', 'text_domain' ),
                'view_item'     => __( 'View Category', 'text_domain' ),
                'view_items'    => __( 'View Categories', 'text_domain' )
            ];

            $args = [
                'labels'            => $labels,
                'hierarchical'      => false,
                'public'            => true,
                'show_ui'           => true,
                'show_admin_column' => true,
                'show_in_nav_menus' => false,
                'show_tagcloud'     => false,
                'show_in_rest'      => true
            ];
            register_taxonomy( 'sport_tax', ['sport'], $args );
        }
    }

    new CptSport();
}
