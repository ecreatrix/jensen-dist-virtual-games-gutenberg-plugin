<?php
namespace jg\Plugin\Gutenberg;

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {exit;}
if ( ! class_exists( Assets::class ) ) {
	class Assets {
		public function __construct() {
			add_action( 'wp_enqueue_scripts', [$this, 'frontend'] );

			add_action( 'enqueue_block_editor_assets', [$this, 'blocks'] );
			add_action( 'rest_api_init', [$this, 'register_routes'] );
		}

		// Enqueue local and remote assets
		public function add_assets( $assets = false, $type = false ) {
			if ( ! $assets || ! is_array( $assets ) ) {
				return;
			}

			foreach ( $assets as $item ) {
				// Name required, skip if it is not present
				if ( ! array_key_exists( 'name', $item ) ) {
					continue;
				}

				$name  = $item['name'];
				$asset = null;

				// Figure out if it's a script or a style
				$script = false;
				$style  = false;
				if ( 'js' === $type || strpos( $name, '.js' ) !== false ) {
					$script = true;
				} else if ( 'css' === $type || strpos( $name, '.css' ) !== false ) {
					$style = true;
				}

				// Get local file or remote link
				if ( array_key_exists( 'link', $item ) && strpos( $item['link'], 'http' ) !== false ) {
					$asset = $item['link'];
				} else if ( $style ) {
					$asset = 'assets/styles/' . $item['name'] . '.css';
				} else if ( $script ) {
					$asset = 'assets/scripts/' . $item['name'] . '.js';
				}

				// Set version for cache busting
				$version     = null;
				$path_prefix = plugin_dir_path( __DIR__ );
				$uri_prefix  = plugin_dir_url( __DIR__ );
				if ( file_exists( $path_prefix . $asset ) ) {
					$version = filemtime( $path_prefix . $asset );
					$uri     = $uri_prefix . $asset;
				} else if ( array_key_exists( 'link', $item ) ) {
					$uri = $item['link'];
				} else {
					return;
				}

				$dependencies = [];
				if ( array_key_exists( 'dependencies', $item ) && is_array( $item['dependencies'] ) ) {
					$dependencies = $item['dependencies'];
				}

				// Add prefix before name
				if ( ! array_key_exists( 'prefix', $item ) ) {
					$item['prefix'] = 'jg-gutenberg-';
				}

				$name = $item['prefix'] . $name;

				// Add parameters inline for use by scripts
				if ( array_key_exists( 'script_object', $item ) && array_key_exists( 'parameters', $item ) ) {
					wp_localize_script( $name, $item['script_object'], $item['parameters'] );
				}

				// Enqueue file/link
				if ( $script ) {
					wp_enqueue_script( $name, $uri, $dependencies, $version, true );
				} else if ( $style ) {
					wp_enqueue_style( $name, $uri, $dependencies, $version );
				}
			}
		}

		public function blocks() {
			$scripts = [
				[
					'name'         => 'manifest',
					'dependencies' => ['jquery'],
				], [
					'name'         => 'vendor',
					'dependencies' => ['jquery'],
				], [
					'name'         => 'blocks',
					'dependencies' => ['lodash', 'wp-a11y', 'wp-api-fetch', 'wp-block-editor', 'wp-block-library', 'wp-blocks', 'wp-components', 'wp-compose', 'wp-core-data', 'wp-data', 'wp-data-controls', 'wp-editor', 'wp-element', 'wp-hooks', 'wp-i18n', 'wp-keyboard-shortcuts', 'wp-keycodes', 'wp-media-utils', 'wp-notices', 'wp-plugins', 'wp-polyfill', 'wp-primitives', 'wp-url', 'wp-viewport'], //, 'jg-gutenberg-blocks.vendor-js'
				],
			];

			$this->add_assets( $scripts, 'js' );

			$styles = [
				[
					'name' => 'editor',
				],
			];

			$this->add_assets( $styles, 'css' );
		}

		public function frontend() {
			$scripts = [
				'main' => [
					'name'         => 'app',
					'dependencies' => ['jquery'],
				],
			];

			$this->add_assets( $scripts, 'js' );

			$styles = [
				[
					'name' => 'app',
				],
			];

			$this->add_assets( $styles, 'css' );
		}

		public static function get_forms( \WP_REST_Request $request ) {
			$all_forms = Ninja_Forms()->form()->get_forms();

			$forms = [];
			foreach ( $all_forms as $form ) {
				$forms[] = [
					'value' => $form->get_id(),
					'label' => $form->get_settings()['title'],
				];
			}
			//return $forms;
			return json_encode( $forms );
		}

		public function register_routes() {
			\register_rest_route( 'jensen/v1', '/ninja/forms/', [
				'methods'             => \WP_REST_Server::READABLE,
				'callback'            => [$this, 'get_forms'],
				'permission_callback' => function () {
					return true;
					return current_user_can( 'edit_post' );
				},
			] );
		}
	}

	new Assets();
}