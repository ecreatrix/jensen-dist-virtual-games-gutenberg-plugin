// External dependencies

// WordPress dependencies
const { __ } = wp.i18n

const {
    InspectorControls,
    withColors,
    PanelColorSettings,
    useBlockProps,
} = wp.blockEditor

const {
    PanelBody, 
} = wp.components

const { compose } = wp.compose

import { pin } from '@wordpress/icons'
// Other Dependencies

// Internal dependencies
import { getNinjaForms } from '../components/forms'

function Signin ( {
    setAttributes,
    attributes,
    attributes: {
        bg,
        primaryLogo,
        secondaryLogo,
        nfSignin,
    },
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,
    formSelectOptions,
    formLoaded
} ) {
    const blockProps = useBlockProps( )

    return (
        <>
            <InspectorControls key="inspector">
                <PanelColorSettings
                    title={ __( 'Background Color' ) }
                    initialOpen={ true }
                    colorSettings={ [
                        {
                            value: textColor.color,
                            onChange: setTextColor,
                            label: __( 'Text color' ),
                        },
                        {
                            value: backgroundColor.color,
                            onChange: setBackgroundColor,
                            label: __( 'Background color' ),
                        },
                    ] }
                >
                </PanelColorSettings>
            </InspectorControls>

            <div { ...blockProps }><h3 className="title">Will display form: { nfSignin }</h3></div>
        </>
    )
}

export default compose(
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( Signin )