// Blocks
import activityHeading from './activity-heading'
import activityInfo from './activity-info'
import activityCategories from './activity-categories'
import activityLeaderboard from './activity-leaderboard'

import pageHeading from './page-heading'

import guestbook from './guestbook'

import setUserData from './user-set-user-data'
import register from './user-register'
import profile from './user-profile'
import signin from './user-sign-in'

const {
	registerBlockType,
} = wp.blocks;

// Register blocks
[ 
	activityHeading,
	activityInfo,
	activityCategories,
	activityLeaderboard,

	pageHeading,

	guestbook,

	signin,
	setUserData,
	register,
	profile,
].forEach( ( { metadata, settings } ) => {
	//console.log(metadata.title)
	registerBlockType( metadata, settings )
} )