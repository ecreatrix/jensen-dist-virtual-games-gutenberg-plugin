// External dependencies

// WordPress dependencies
const { __ } = wp.i18n

const {
    useState
} = wp.element

const {
    BlockControls,
    BlockIcon,
    InspectorControls,
    withColors,
    PanelColorSettings,
    useBlockProps,
} = wp.blockEditor

const {
    ServerSideRender,
} = wp

const {
    Placeholder,
    Spinner,
    PanelBody, 
    SelectControl,
    Disabled
} = wp.components

const { compose } = wp.compose

const {
    withSelect,
    select,
} = wp.data

import { pin } from '@wordpress/icons'

// Other Dependencies

// Internal dependencies
import { getFormName, getNinjaForms } from '../components/forms'

function FinishRegistration ( {
    setAttributes,
    attributes: { nfRegistration },
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,
    formSelectOptions,
    formLoaded
} ) {
    let formsInspector
    if ( formLoaded ) {
        formsInspector =<PanelBody
            title={ __( 'Ninja Form' ) }
            initialOpen={ false }
        >
            <SelectControl
                label={ __( 'Registration' ) }
                value={ nfRegistration }
                onChange={ ( value ) =>
                    setAttributes( { nfRegistration: value } )
                }
                options={ formSelectOptions }
            />
        </PanelBody>
    } else {
        return (
            <>
                <Placeholder icon={ pin } label={ __( 'Activity Info' ) }>
                    <Spinner />
                </Placeholder>
            </>
        )
    }

    let registrationFormLabel = getFormName( nfRegistration, formSelectOptions )

    const blockProps = useBlockProps( )

    return (
        <>
            <InspectorControls key="inspector">
                <PanelColorSettings
                    title={ __( 'Background Color' ) }
                    initialOpen={ true }
                    colorSettings={ [
                        {
                            value: textColor.color,
                            onChange: setTextColor,
                            label: __( 'Text color' ),
                        },
                        {
                            value: backgroundColor.color,
                            onChange: setBackgroundColor,
                            label: __( 'Background color' ),
                        },
                    ] }
                >
                </PanelColorSettings>
                { formsInspector }
            </InspectorControls>


            <div { ...blockProps }><h3 className="title">Will display Registration Form - { registrationFormLabel }</h3></div>
        </>
    )
}

export default compose(
    withSelect( ( select, props ) => {
        const [ formSelectOptions, setForms ] = useState( null )
        const [ formLoaded, setFormsformLoaded ] = useState( false )
        if( !formLoaded ) {
            getNinjaForms( {setFormsformLoaded, setForms} )
        }

        return {
            formSelectOptions,
            formLoaded
        }
    } ),
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( FinishRegistration )
