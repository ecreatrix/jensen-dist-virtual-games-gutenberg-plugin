// External dependencies
import classnames from 'classnames'

// WordPress dependencies
const { __ } = wp.i18n

const { compose } = wp.compose

const {
    InspectorControls,
    withColors,
    PanelColorSettings,
    useBlockProps,
} = wp.blockEditor

const {
    Placeholder,
    Spinner,
    SelectControl,
    PanelBody
} = wp.components

const { apiFetch } = wp

import { pin } from '@wordpress/icons'

const {
    withSelect,
    select,
    useSelect,
} = wp.data

const {
    useState
} = wp.element

// Other Dependencies

// Internal dependencies
import { categoryPanel, categoryResults } from '../components/category'
import { getNinjaForms } from '../components/forms'

function ActivityInfo ( {
    isResolvingRequest,
    activityCategories,
    setAttributes,
    attributes: { category, nfPost },
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,
    className,
    formSelectOptions,
    formLoaded
} ) {
    let info = (categories, categoryAttribute) => {
        let info = categories && categories.map( ( category, index ) => {
            if( category.slug === categoryAttribute ) {
                return category.jg_sport_tax_info.content
            }
        } )

        info = categoryResults(categoryAttribute, info, false)

        return (
            <div className="wp-block-cover__inner-container row" dangerouslySetInnerHTML={{__html: info}}></div>
        )
    }

    let inspectorControls = <>
        { categoryPanel(setAttributes, category, activityCategories) }
        <PanelColorSettings
            title={ __( 'Background Color' ) }
            initialOpen={ true }
            colorSettings={ [
                {
                    value: textColor.color,
                    onChange: setTextColor,
                    label: __( 'Text color' ),
                },
                {
                    value: backgroundColor.color,
                    onChange: setBackgroundColor,
                    label: __( 'Background color' ),
                },
            ] }
        >
        </PanelColorSettings>
    </>

    if ( formLoaded ) {
        inspectorControls = <>
            { inspectorControls }
            <PanelBody
                title={ __( 'Ninja Forms' ) }
                initialOpen={ false }
            >
                <SelectControl
                    label={ __( 'Add Activity' ) }
                    value={ nfPost }
                    onChange={ ( value ) => setAttributes( { nfPost: value } ) }
                    options={ formSelectOptions }
                />
            </PanelBody>
        </>
    }

    const style = {
        backgroundColor: backgroundColor && backgroundColor.color,
    }

    const classes = classnames( 
        'sport-info', 'wp-block-cover', className, {
        [ backgroundColor.class ]: backgroundColor.class,
    } )

    const blockProps = useBlockProps( {
        className: classes,
        style,
    })

    if ( isResolvingRequest ) {
        return (
            <>
                <Placeholder icon={ pin } label={ __( 'Activity Info' ) }>
                    <Spinner />
                </Placeholder>
            </>
        )
    }

    return (
        <>  
            <InspectorControls key="inspector">
                { inspectorControls }
            </InspectorControls>
            <div { ...blockProps }>{ info(activityCategories, category) }</div>
        </>
    )
}

export default compose(
    withSelect( ( select, props ) => {
        const { getEntityRecords } = select( 'core' )
        const { isResolving } = select( 'core/data' )
        const query = { per_page: -1, form_id: props.attributes.nfPost }

        let activityCategories = getEntityRecords( 'taxonomy', 'sport_tax', query )
        let isResolvingRequest = isResolving( 'core', 'getEntityRecords', [
            'taxonomy',
            'sport_tax',
            query,
        ] ) 

        const [ formSelectOptions, setForms ] = useState( null )
        const [ formLoaded, setFormsformLoaded ] = useState( false )
        
        if( !formLoaded ) {
            getNinjaForms( {setFormsformLoaded, setForms} )
        }

        return {
            activityCategories,
            isResolvingRequest,
            formSelectOptions,
            formLoaded
        }
    } ),
    withColors( 'backgroundColor', 'textColor' ),
)( ActivityInfo )
