// External dependencies
import classnames from 'classnames'

// WordPress dependencies
const { __ } = wp.i18n

const { compose } = wp.compose

const {
    InnerBlocks,
    InspectorControls,
    withColors,
    PanelColorSettings,
    useBlockProps,
} = wp.blockEditor

import { pin } from '@wordpress/icons'

const {
    Placeholder,
    Spinner,
    PanelBody, 
    SelectControl, 
    RangeControl,
} = wp.components

const {
    withSelect,
    select,
    useSelect,
} = wp.data

const { apiFetch } = wp

const {
    useState
} = wp.element

// Other Dependencies

// Internal dependencies

function ActivityLeaderboard ({
    leaderboardLoaded,
    leaderboard,
    setAttributes,
    attributes: {
        ages,
        limit,
    },
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,
    className
} ) {
    let inspectorControls = <>
        <InspectorControls key="inspector">
            <PanelColorSettings
                title={ __( 'Background Color' ) }
                initialOpen={ true }
                colorSettings={ [
                    {
                        value: textColor.color,
                        onChange: setTextColor,
                        label: __( 'Text color' ),
                    },
                    {
                        value: backgroundColor.color,
                        onChange: setBackgroundColor,
                        label: __( 'Background color' ),
                    },
                ] }
            >
            </PanelColorSettings>
            <PanelBody title={ __( 'Content' ) }>
                <SelectControl
                    label={ __( 'Age Group' ) }
                    value={ ages }
                    options={ [
                        { value: false, label: '--' },
                        { value: '0-12', label: '0-12' },
                        { value: '13-19', label: '13-19' },
                        { value: '20+', label: '20+' },
                    ] }
                    multiple
                    onChange={ ( value ) => setAttributes( { ages: value } ) }
                />
                <RangeControl
                    label={ __( 'Max number of participants' ) }
                    value={ limit }
                    onChange={ ( value ) => setAttributes( { limit: value } ) }
                    min={ 1 }
                    max={ 15 }
                    type="stepper"
                />
            </PanelBody>
        </InspectorControls>
    </>

    if ( !leaderboardLoaded ) {
        return (
            <>
                { inspectorControls }
                <Placeholder icon={ pin } label={ __( 'Leaderboard' ) }>
                    <Spinner />
                </Placeholder>
            </>
        )
    }

    const classes = classnames( 
        'aligncenter', className, {
    } )

    const blockProps = useBlockProps( {
        className: classes,
    })

    if ( ! ages || ages === 'false' ) {
        return <>
            { inspectorControls }
            <h1 { ...blockProps }>Select an age range</h1>
        </>
    }
//console.log(leaderboard)

    return (
        <>
            { inspectorControls }
            <div { ...blockProps } dangerouslySetInnerHTML={{__html: leaderboard }} />
        </>
    )
}

export default compose(
    withSelect( ( select, props ) => {
        const [ leaderboard, setLeaderboard ] = useState( null )
        const [ leaderboardLoaded, setLeaderboardLoaded ] = useState( false )

        //if(!leaderboardLoaded) {
            apiFetch( {
                path: `/jensen/v1/sports/leaderboard`,
                method: 'POST',
                data: { attributes: props.attributes },
            } ).then( ( result ) => {
                if ( ! result ) {
                    console.log( 'Error' )
                    console.log( result )
                }

                setLeaderboard( result )
                setLeaderboardLoaded( true )

                //return {
                //    formSelectOptions: setForms( parsed ),
                //    formLoaded: setFormsformLoaded( true )
                //}
            } ).catch( () => {
                setLeaderboard( [] )
                setLeaderboardLoaded( true )

                //return {
                //    formSelectOptions: setForms( [] ),
                //    formLoaded: setFormsformLoaded( true )
                //}
            } )
        //}

        return {
            leaderboard,
            leaderboardLoaded,
        }
    } ),
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( ActivityLeaderboard )
