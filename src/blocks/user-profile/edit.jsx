// External dependencies

// WordPress dependencies
import { __ } from '@wordpress/i18n'

const {
    BlockControls,
    BlockIcon,
    InspectorControls,
    withColors,
    PanelColorSettings,
    useBlockProps,
} = wp.blockEditor

import apiFetch from '@wordpress/api-fetch'

const {
    Placeholder,
    Spinner,
    SelectControl,
    PanelBody
} = wp.components

import { compose } from '@wordpress/compose'

const {
    withSelect,
    select,
    useSelect,
} = wp.data

const {
    useState
} = wp.element

import { pin } from '@wordpress/icons'

// Other Dependencies

// Internal dependencies
import { getFormName, getNinjaForms } from '../components/forms'

function Profile ({
    setAttributes,
    attributes: {
        nfProfile,
        nfPassword,
        nfFamily,
        nfComment
    },
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,
    formSelectOptions,
    formLoaded
} ) {
    let inspectorControls = <InspectorControls key="inspector">
        <PanelColorSettings
            title={ __( 'Background Color' ) }
            initialOpen={ true }
            colorSettings={ [
                {
                    value: textColor.color,
                    onChange: setTextColor,
                    label: __( 'Text color' ),
                },
                {
                    value: backgroundColor.color,
                    onChange: setBackgroundColor,
                    label: __( 'Background color' ),
                },
            ] }
        >
        </PanelColorSettings>
    </InspectorControls>

    if( formLoaded ) {
        inspectorControls = <>
            { inspectorControls }
            <PanelBody
                title={ __( 'Ninja Forms' ) }
                initialOpen={ false }
            >
                <SelectControl
                    label={ __( 'Select Profile Form', 'jensen' ) }
                    value={ nfProfile } 
                    onChange={ ( value ) => setAttributes( { nfProfile: value } ) }
                    options={ formSelectOptions }
                />
                <SelectControl
                    label={ __( 'Select Password Form', 'jensen' ) }
                    value={ nfPassword } 
                    onChange={ ( value ) => setAttributes( { nfPassword: value } ) }
                    options={ formSelectOptions }
                />
                <SelectControl
                    label={ __( 'Select Add Family Form', 'jensen' ) }
                    value={ nfFamily } 
                    onChange={ ( value ) => setAttributes( { nfFamily: value } ) }
                    options={ formSelectOptions }
                />
                <SelectControl
                    label={ __( 'Select Add Comment Form', 'jensen' ) }
                    value={ nfComment } 
                    onChange={ ( value ) => setAttributes( { nfComment: value } ) }
                    options={ formSelectOptions }
                />
            </PanelBody>
        </>   
    } else {
        return (
            <>
                <Placeholder icon={ pin } label={ __( 'Profile Info' ) }>
                    <Spinner />
                </Placeholder>
            </>
        )
    }

    let profileFormLabel = getFormName(nfProfile, formSelectOptions)
    let passwordFormLabel = getFormName(nfPassword, formSelectOptions)
    let familyFormLabel = getFormName(nfFamily, formSelectOptions)
    let commentFormLabel = getFormName(nfComment, formSelectOptions)

    const blockProps = useBlockProps( )

    return (
        <>  <InspectorControls key="inspector">
                { inspectorControls }
            </InspectorControls>
            <div { ...blockProps }>
                <h3 className="title">Will display profile</h3>
                <div>Profile Form - { profileFormLabel }</div>
                <div>Password Form - { passwordFormLabel }</div>
                <div>Family Entry Form - { familyFormLabel }</div>
                <div>Guestbook Entry Form - { commentFormLabel }</div>
            </div>
        </>
    )    
}

export default compose(
    withSelect( ( select, props ) => {
        const [ formSelectOptions, setForms ] = useState( null )
        const [ formLoaded, setFormsformLoaded ] = useState( false )
        if( !formLoaded ) {
            getNinjaForms( {setFormsformLoaded, setForms} )
        }

        return {
            formSelectOptions,
            formLoaded
        }
    } ),
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( Profile )