// External dependencies
import classnames from 'classnames'

// WordPress dependencies
const { __ } = wp.i18n

const {
    InspectorControls,
    withColors,
    PanelColorSettings,
    useBlockProps,
    Disabled,
} = wp.blockEditor

import { pin } from '@wordpress/icons'

const {
    Placeholder,
    Spinner,
    guestbookLoaded,
    guestbook,
    PanelBody, 
    SelectControl,
} = wp.components

const { compose } = wp.compose

const {
    withSelect,
    select,
    useSelect,
} = wp.data

const { apiFetch } = wp

const {
    useState
} = wp.element

// Other Dependencies

// Internal dependencies
import { getNinjaForms } from '../components/forms'

function Guestbook ({
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,

    className,

    attributes: { nfComment },
    setAttributes,

    guestbook,
    guestbookLoaded,
    formSelectOptions,
    formLoaded
} ) {
    let inspectorControls = <PanelColorSettings
        title={ __( 'Background Color' ) }
        initialOpen={ true }
        colorSettings={ [
            {
                value: textColor.color,
                onChange: setTextColor,
                label: __( 'Text color' ),
            },
            {
                value: backgroundColor.color,
                onChange: setBackgroundColor,
                label: __( 'Background color' ),
            },
        ] }
    >
    </PanelColorSettings>

    if ( formLoaded ) {
        inspectorControls = <>
            { inspectorControls }
            <PanelBody
                title={ __( 'Ninja Forms' ) }
                initialOpen={ false }
            >
                <SelectControl
                    label={ __( 'Add guestbook form' ) }
                    value={ nfComment }
                    onChange={ ( value ) => setAttributes( { nfComment: value } ) }
                    options={ formSelectOptions }
                />
            </PanelBody>
        </>
    }

    if ( !guestbookLoaded ) {
        return (
            <>
                { inspectorControls }
                <Placeholder icon={ pin } label={ __( 'Guestbook' ) }>
                    <Spinner />
                </Placeholder>
            </>
        )
    }

    const classes = classnames( 
        'guestbook', className, {
    } )

    const blockProps = useBlockProps( {
        className: classes,
    })

    return <>
        <InspectorControls key="inspector">
            { inspectorControls }
        </InspectorControls>
        <div { ...blockProps } dangerouslySetInnerHTML={{__html: guestbook }} />
    </>
}

export default compose(
    withSelect( ( select, props ) => {
        const [ guestbook, setGuestbook ] = useState( null )
        const [ guestbookLoaded, setGuestbookLoaded ] = useState( false )

        if(!guestbookLoaded) {
            apiFetch( {
                path: `/jensen/v1/posts/guestbook`,
            } ).then( ( result ) => {
                if ( ! result ) {
                    console.log( 'Error' )
                    console.log( result )
                }

                setGuestbook( result )
                setGuestbookLoaded( true )

                //return {
                //    formSelectOptions: setForms( parsed ),
                //    formLoaded: setFormsformLoaded( true )
                //}
            } ).catch( () => {
                setGuestbook( [] )
                setGuestbookLoaded( true )

                //return {
                //    formSelectOptions: setForms( [] ),
                //    formLoaded: setFormsformLoaded( true )
                //}
            } )
        }

        const [ formSelectOptions, setForms ] = useState( null )
        const [ formLoaded, setFormsformLoaded ] = useState( false )

        if( !formLoaded ) {
            getNinjaForms( {setFormsformLoaded, setForms} )
        }

        return {
            guestbook,
            guestbookLoaded,
            formSelectOptions,
            formLoaded
        }
    } ),
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( Guestbook )
