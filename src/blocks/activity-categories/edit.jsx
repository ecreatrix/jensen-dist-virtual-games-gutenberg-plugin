// External dependencies

// WordPress dependencies
const { __ } = wp.i18n

const { compose } = wp.compose

const {
    Component,
    Fragment
} = wp.element

const {
    InnerBlocks,
    InspectorControls,
    InspectorAdvancedControls,
    withColors,
    useBlockProps,
} = wp.blockEditor

const {
    Placeholder,
    Spinner,
    PanelBody,
    PanelRow,
} = wp.components

import { pin } from '@wordpress/icons'

const {
    withSelect,
    select,
} = wp.data

// Other Dependencies

// Internal dependencies
import { multipleCategoriesPanel, categoryResults } from '../components/category'

function ActivityCategory ( {
    isRequesting,
    activityCategories,
    setAttributes,
    attributes: { categories },
} ) {
    let renderCategoriesItem = (category, index) => {
        return <div key={ index } className="" dangerouslySetInnerHTML={ { __html: category.jg_sport_tax } } />
    }
    
    let inspectorControls = <>
        <InspectorControls key="inspector">
            { multipleCategoriesPanel(setAttributes, categories, activityCategories ) }
        </InspectorControls>
    </>

    if ( isRequesting ) {
        return (
            <>
                { inspectorControls }
                <Placeholder icon={ pin } label={ __( 'Activity Categories' ) }>
                    <Spinner />
                </Placeholder>
            </>
        )
    }

    const blockProps = useBlockProps( {
        className: 'sport-categories',
    })

    return (
        <>
            { inspectorControls }
            <div { ...blockProps }>
                <div className="activities-row">
                    { activityCategories && activityCategories.map( ( category, index ) =>
                        renderCategoriesItem( category, index )
                    ) }
                </div>
            </div>
        </>
    )
}

export default compose(
    withSelect( ( select ) => {
        const { getEntityRecords } = select( 'core' )
        const { isResolving } = select( 'core/data' )
        const query = { per_page: -1 }

        return {
            activityCategories: getEntityRecords( 'taxonomy', 'sport_tax', query ),
            isRequesting: isResolving( 'core', 'getEntityRecords', [
                'taxonomy',
                'jg_sport_tax',
                query,
            ] ),
        }
    } ),
    withColors( 'backgroundColor', 'overlayColor', 'textColor' ),
)( ActivityCategory )
