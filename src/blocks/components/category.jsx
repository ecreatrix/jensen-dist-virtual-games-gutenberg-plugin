
import Select from 'react-select'

// WordPress dependencies
const { __ } = wp.i18n

const {
    Placeholder,
    Spinner,
    PanelBody,
    SelectControl,
} = wp.components

export function categoryPanel(setAttributes, category, sportCategories) {
    let options = sportCategories ? sportCategories.map( ( signleCategory ) => {
        return {
            value: signleCategory.slug, 
            label: signleCategory.name,
        }
    } ) : []

    return (
        <PanelBody title={ __( 'Content' ) }>
            <SelectControl
                label={ __( 'Activity Category' ) }
                value={ category }
                options={ [{
                        value: false, label: '--',
                    }, ...options
                ] }
                onChange={ ( value ) => setAttributes( { category: value } ) }
            />
        </PanelBody>
    )
}
export function multipleCategoriesPanel(setAttributes, categories, sportCategories) {
    let options = sportCategories ? sportCategories.map( ( signleCategory ) => {
        return {
            value: signleCategory.slug, 
            label: signleCategory.name,
        }
    } ) : []

    return (
        <PanelBody
            title={ __( 'Content', 'text-domain' ) }
        >
            <SelectControl
                label={ __( 'Select Categories' ) }
                value={ categories }
                options={ [{
                        value: false, label: '--',
                    }, ...options
                ] }
                multiple
                onChange={ ( value ) => setAttributes( { categories: value } ) }
            />
         </PanelBody>
    )
}
export function categoryResults(categoryAttribute, posts, requireActivities = true) {
    if(posts) {
        posts = posts.filter(item => item)
    }

    if( !categoryAttribute || categoryAttribute === 'false' ) {
        posts = `Please select category from block settings.`
    } else if( requireActivities && posts && posts.length == 0 ) {
        posts = `No sports are available yet in the ${ categoryAttribute } category.`
    }

    return posts
}
