const { apiFetch } = wp

export const getNinjaForms = ( {setFormsformLoaded, setForms} ) => {
    apiFetch( {
        path: `/jensen/v1/ninja/forms`,
    } ).then( ( result ) => {
        if ( ! result ) {
            console.log( 'Error' )
            console.log( result )
        }

        let parsed = JSON.parse( result )
        parsed = [
            { label: 'Choose Form', value: false },
            ...parsed
        ]

        setForms( parsed )
        setFormsformLoaded( true )
    } ).catch( () => {
        setForms( [] )
        setFormsformLoaded( true )
    } )
}

export const getFormName = ( key, formSelectOptions ) => {
    let label = 'Form not found';

    formSelectOptions && formSelectOptions.map( ( form, index ) => {
        let formKey = '' + form.value

        if( formKey === key ) {
            label = form.label
        }
    })

    return label;
}