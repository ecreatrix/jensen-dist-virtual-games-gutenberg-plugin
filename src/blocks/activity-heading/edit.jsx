// External dependencies
import classnames from 'classnames'

// WordPress dependencies
const { __ } = wp.i18n

const { compose } = wp.compose

const {
    BlockControls,
    BlockIcon,
    InspectorControls,
    withColors,
    PanelColorSettings,
    useBlockProps,
} = wp.blockEditor

const {
    Placeholder,
    Spinner,
} = wp.components

import { pin } from '@wordpress/icons'

const {
    withSelect,
    select,
} = wp.data

// Other Dependencies

// Internal dependencies
import { categoryPanel, categoryResults } from '../components/category'

function ActivityHeading ( {
    isRequesting,
    activityCategories,
    setAttributes,
    attributes: { category },
    backgroundColor,
    setBackgroundColor,
    textColor,
    setTextColor,
    className
} ) {
    let heading = (categories, categoryAttribute) => {
        let heading = categories && categories.map( ( category, index ) => {
            if( category.slug === categoryAttribute ) {
                return category.jg_sport_tax_heading.content
            }
        } )

        heading = categoryResults(categoryAttribute, heading, false)

        return (
            <div className="wp-block-cover__inner-container row" dangerouslySetInnerHTML={{__html: heading}}></div>
        )
    }

    let inspectorControls = <>
        <InspectorControls key="inspector">
            { categoryPanel(setAttributes, category, activityCategories) }
        <PanelColorSettings
            title={ __( 'Background Color' ) }
            initialOpen={ true }
            colorSettings={ [
                {
                    value: textColor.color,
                    onChange: setTextColor,
                    label: __( 'Text color' ),
                },
                {
                    value: backgroundColor.color,
                    onChange: setBackgroundColor,
                    label: __( 'Background color' ),
                },
            ] }
        >
        </PanelColorSettings>
        </InspectorControls>
    </>

    if ( isRequesting ) {
        return (
            <>
                { inspectorControls }
                <Placeholder icon={ pin } label={ __( 'Activity Heading' ) }>
                    <Spinner />
                </Placeholder>
            </>
        )
    }

    const style = {
        backgroundColor: backgroundColor && backgroundColor.color,
    }

    const classes = classnames( 
        'wp-block-cover', 'sport-heading', className, {
        [ backgroundColor.class ]: backgroundColor.class,
    } )

    const blockProps = useBlockProps( {
        className: classes,
        style,
    })

    return (
        <>
            { inspectorControls }
            <div { ...blockProps }>{ heading(activityCategories, category) }</div>
        </>
    )
}

export default compose(
    withSelect( ( select ) => {
        const { getEntityRecords } = select( 'core' )
        const { isResolving } = select( 'core/data' )
        const query = { per_page: -1 }

        return {
            activityCategories: getEntityRecords( 'taxonomy', 'sport_tax', query ),
            isRequesting: isResolving( 'core', 'getEntityRecords', [
                'taxonomy',
                'sport_tax',
                query,
            ] ),
        }
    } ),
    withColors( 'backgroundColor', 'textColor' ),
)( ActivityHeading )
