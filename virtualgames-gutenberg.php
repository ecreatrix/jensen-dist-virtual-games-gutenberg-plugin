<?php
/**
 * @wordpress-plugin
 * Plugin Name: Virtual Games Gutenberg blocks
 * Plugin URI: https://jensengroup.ca/
 * Description: Gutenberg blocks for theme use
 * Version:     2022.3
 * Author:      Jensen Group
 * License:     GPL-2.0+
 * License URI: http:// www.gnu.org/licenses/gpl-2.0.txt
 * @package   jg-Gutenberg
 *
 * @author    Jensen Group
 * @copyright 2021 Jensen Group
 * @license   GPL-2.0+
 */
namespace jg\Plugin\Gutenberg;

defined( __NAMESPACE__ . '\URI' ) || define( __NAMESPACE__ . '\URI', plugin_dir_url( __FILE__ ) );
defined( __NAMESPACE__ . '\PATH' ) || define( __NAMESPACE__ . '\PATH', plugin_dir_path( __FILE__ ) );

$files = [
    'blocks/helpers',
    'assets',
    'cpt-guestbook', 'cpt-sport',
    'blocks/misc/page-heading', 'blocks/misc/guestbook',
    'blocks/profile/app', 'blocks/profile/family', 'blocks/profile/sport',
    'blocks/sport/category', 'blocks/sport/heading', 'blocks/sport/info', 'blocks/sport/leaderboard',
    'blocks/registration/complete', 'blocks/registration/register', 'blocks/registration/sign-in',
];

foreach ( $files as $file ) {
    $file = plugin_dir_path( __FILE__ ) . 'app/' . $file . '.php';

    if ( file_exists( $file ) ) {
        require_once $file;
    }
}